/**
 * engine.js is an engine for decoding and formatting web content
 * in order to make reading easier for dyslexic people.
 * This module is part of DysAide.
 * 
 * The implemented phoneme-reading algorithm is a modification and
 * completion of the one from Marie-Pierre Brungard’s LireCouleur: 
 * http://lirecouleur.arkaline.fr
 *
 * @author   Bunker D
 * @contact  contact@bunkerd.fr
 * @version  0.1
 * @since    0.1
 * @license  GNU GPLv3
 *
 * GNU General Public Licence (GPL) version 3
 *
 * DysAide is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * DysAide is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with
 * DysAide; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA4z
 */


/*      /██\                                                    *
 *     / ██ \    La page HTML utilisant ce code doit contenir   *
 *    /  ▀▀  \   <meta charset="UTF-8"> dans son <head>.        *
 *   /___██___\                                                 */

/*jshint evil: true */

/* Liste des phonèmes :
 *
 *    Label  Texte  SAMPA  Lexi.  Exemple(s)
 *      p      p      -      -      papa
 *      b      b      -      -      bon
 *      t      t      -      -      tu
 *      d      d      -      -      du
 *      k      c      -      -      coq, qui
 *      g      g      -      -      gare
 *      f      f      -      -      fil
 *      v      v      -      -      vélo
 *      s      s      -      -      sur
 *      z      z      -      -      zoo
 *      S      ch     -      -      chat
 *      Z      j      -      -      gens
 *      j      y      -      -      fille, crayon
 *      m      m      -      -      mamie
 *      n      n      -      -      nul
 *      l      l      -      -      lilas
 *      r      r      -      -      rat
 *      w      w      -      -      web
 *      N      gn     J      N      vigne
 *      G      ng     N      G      camping
 *      x      ks     ks     ks     texte
 *      X      gz     gz     gz     exact
 *      i      i      -      -      il
 *      e      é      -      -      blé
 *      E      è      -      -      père, sel
 *      a      a      -      -      ta
 *      O      O      -      -      comme
 *      o      o      -      -      gros, beau
 *      y      u      -      -      tu
 *      2      e      -      -      deux
 *      9      e      -      -      neuf
 *      q      e      @      °      justement
 *      u      ou     -      -      nous, fou
 *      3      oi     wa     wa     toi, oiseau
 *      A      an     a~     @      grand
 *      0      on     o~     §      bon
 *      1      un     9~     1      brun
 *      5      in     e~     5      vin
 *      H      (muet)        #
 *      -      ponctuation
 *      I      séparateur inclusif
 *
 *   SAMPA :     https://fr.wikipedia.org/wiki/Symboles_SAMPA_français
 *   Lexique :   http://www.lexique.org/moteur/Open.php?base[0]=lexique3&nbfields=8&Open_Lexique=Recherche
 */


/* Classe Phoneme, encodant une phonème (écriture et son) */
function dysaide_Phoneme(phon, lett) {
	this.phoneme = phon;
	this.lettres = lett;
}

/* Alphabet phonétique ascii : voir http://www.icp.inpg.fr/ICP/avtts/phon.fr.html
 * Base de donnée des mots français, avec prononciation : http://www.lexique.org/moteur
 */
function dysaide_Engine(options, testmode) {
    // Options
    this.testmode = (testmode) ? true : false;
    this.set_options(options);
    // Mise en place de l'automate si nécessaire
    this.automate();
}

/* Définit this.options :
   Si l'argument est un object, il est pris pour valeur.
   Sinon, les options par défaut sont utilisées. */
dysaide_Engine.prototype.set_options = function(options) {
    this.options = options ||
        {
            'gene' : {
                'actif'       :  true,
                'police'      : [true , 'Lexie Readable' ],
                'taille'      : [false, 18  ],
                'esp_lettres' : [true , 0.2 ],
                'esp_mots'    : [true , 0.7 ],
                'esp_lignes'  : [true , 1   ],
                'diff_lignes' : [false, { 'col' : [false, '#0fa4f6', '#f19500' ] ,
                                          'bg'  : [true , '#ccfeff', '#fff879' ] }],
                'liens'       : [false, { 'col' : [false, '#008d31' ] ,
                                          'bg'  : [true , '#aef4bb' ] }]
            },
            'syll' : {
                'actif' :  false,
                'oral'  : [false],
                'diff'  : [true , { 'nb'  : 3 ,
                                    'col' : [false, '#555af8', '#fc3533', '#16b716' ] ,
                                    'bg'  : [true , '#ccfeff', '#ffb6a6', '#fff879' ] }],
                'sep'   : [false, '-' ]
            },
            'phon' : {
                'actif' :  false,
                'q_fin' : [false],
                'H'     : [true , { 'col' : [true, '#a0a0a0' ], 'bg' : [false, '#dbdbdb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'a'     : [false, { 'col' : [true, '#b56912' ], 'bg' : [false, '#fff9b1' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                '2'     : [true , { 'col' : [true, '#6a0000' ], 'bg' : [false, '#dbbaba' ], 's' : [true ], 'i' : [false], 'g' : [true ] }],
                '9'     : [true , { 'col' : [true, '#6a0000' ], 'bg' : [false, '#dbbaba' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'q'     : [false, { 'col' : [true, '#6a0000' ], 'bg' : [false, '#dbbaba' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'e'     : [true , { 'col' : [true, '#d81422' ], 'bg' : [false, '#ff8db8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'E'     : [true , { 'col' : [true, '#f82eff' ], 'bg' : [false, '#ed97ff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'i'     : [false, { 'col' : [true, '#0fdd00' ], 'bg' : [false, '#bfffc9' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'j'     : [true , { 'col' : [true, '#006c27' ], 'bg' : [false, '#99c697' ], 's' : [false], 'i' : [true ], 'g' : [false] }],
                'o'     : [true , { 'col' : [true, '#1500cf' ], 'bg' : [false, '#d3e8ff' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'O'     : [true , { 'col' : [true, '#1500cf' ], 'bg' : [false, '#d3e8ff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'y'     : [false, { 'col' : [true, '#14ab79' ], 'bg' : [false, '#6aefce' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'u'     : [true , { 'col' : [true, '#0fb0d9' ], 'bg' : [false, '#a7f4fb' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '3'     : [true , { 'col' : [true, '#ecae08' ], 'bg' : [false, '#ffe998' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'A'     : [true , { 'col' : [true, '#4e41ff' ], 'bg' : [false, '#c4d1ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '0'     : [true , { 'col' : [true, '#ff660f' ], 'bg' : [false, '#ffb19f' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '1'     : [true , { 'col' : [true, '#7724bf' ], 'bg' : [false, '#dcb7ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '5'     : [true , { 'col' : [true, '#7724bf' ], 'bg' : [false, '#dcb7ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'b'     : [false, { 'col' : [true, '#bb63d0' ], 'bg' : [false, '#f0ccff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'k'     : [false, { 'col' : [true, '#38f075' ], 'bg' : [false, '#b4ffb0' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'S'     : [false, { 'col' : [true, '#75bc22' ], 'bg' : [false, '#cde8a6' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'd'     : [false, { 'col' : [true, '#e5750a' ], 'bg' : [false, '#f2c395' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'f'     : [false, { 'col' : [true, '#da5db9' ], 'bg' : [false, '#ecb5dd' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'g'     : [false, { 'col' : [true, '#8fc569' ], 'bg' : [false, '#c8e1b7' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'Z'     : [false, { 'col' : [true, '#fd2966' ], 'bg' : [false, '#fd2966' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'N'     : [false, { 'col' : [true, '#e47646' ], 'bg' : [false, '#f3bb8c' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'l'     : [false, { 'col' : [true, '#4f10df' ], 'bg' : [false, '#ad92e9' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'm'     : [false, { 'col' : [true, '#dcbe0a' ], 'bg' : [false, '#f0e394' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'n'     : [false, { 'col' : [true, '#bf8f72' ], 'bg' : [false, '#dec7ba' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'G'     : [false, { 'col' : [true, '#2067e2' ], 'bg' : [false, '#a0bef4' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'p'     : [false, { 'col' : [true, '#3295c3' ], 'bg' : [false, '#9cc9df' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'r'     : [false, { 'col' : [true, '#c88cf4' ], 'bg' : [false, '#e5cdf6' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                's'     : [false, { 'col' : [true, '#19bfad' ], 'bg' : [false, '#a2f8ee' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'z'     : [false, { 'col' : [true, '#0c7ebf' ], 'bg' : [false, '#42b2f8' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                't'     : [false, { 'col' : [true, '#a33238' ], 'bg' : [false, '#d19699' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'v'     : [false, { 'col' : [true, '#dabf16' ], 'bg' : [false, '#f9f6cd' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'w'     : [false, { 'col' : [true, '#4a3bb8' ], 'bg' : [false, '#a7a1dc' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'x'     : [false, { 'col' : [true, '#bf408b' ], 'bg' : [false, '#dea3c6' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'X'     : [false, { 'col' : [true, '#bf408b' ], 'bg' : [false, '#dea3c6' ], 's' : [false], 'i' : [false], 'g' : [true ] }]
            },
            'lett' : {
                'actif' :  false,
                '_'     : [false, { 'col' : [true, '#7b1411' ], 'bg' : [false, '#b25d5d' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'a'     : [false, { 'col' : [true, '#72f472' ], 'bg' : [false, '#c0fac3' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'b'     : [true , { 'col' : [true, '#cb1616' ], 'bg' : [false, '#ffec5e' ], 's' : [false], 'i' : [true ], 'g' : [false] }],
                'c'     : [false, { 'col' : [true, '#ff9900' ], 'bg' : [false, '#ffea72' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'C'     : [false, { 'col' : [true, '#c77700' ], 'bg' : [false, '#eeb988' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'd'     : [true , { 'col' : [true, '#261bcb' ], 'bg' : [false, '#a0d1ff' ], 's' : [false], 'i' : [true ], 'g' : [false] }],
                'e'     : [false, { 'col' : [true, '#0b93d6' ], 'bg' : [false, '#92cfeb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'é'     : [false, { 'col' : [true, '#4c1798' ], 'bg' : [false, '#a186cb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'è'     : [false, { 'col' : [true, '#d740ee' ], 'bg' : [false, '#f2b1fb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'f'     : [false, { 'col' : [true, '#70dc47' ], 'bg' : [false, '#bdebac' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'g'     : [false, { 'col' : [true, '#bc9c4e' ], 'bg' : [false, '#dbcda8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'h'     : [false, { 'col' : [true, '#0daf6a' ], 'bg' : [false, '#8bddba' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'i'     : [false, { 'col' : [true, '#681eff' ], 'bg' : [false, '#ae93ff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'j'     : [false, { 'col' : [true, '#c11d0a' ], 'bg' : [false, '#e0938a' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'k'     : [false, { 'col' : [true, '#cf8b1a' ], 'bg' : [false, '#edcf99' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'l'     : [false, { 'col' : [true, '#00d2bd' ], 'bg' : [false, '#5edce8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'm'     : [false, { 'col' : [true, '#3ca108' ], 'bg' : [false, '#3dff07' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'n'     : [true , { 'col' : [true, '#d1881d' ], 'bg' : [false, '#ffec5e' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'o'     : [false, { 'col' : [true, '#3a8fe3' ], 'bg' : [false, '#aed2f8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'p'     : [true , { 'col' : [true, '#2a8906' ], 'bg' : [false, '#7dff76' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'q'     : [true , { 'col' : [true, '#c90bd1' ], 'bg' : [false, '#faa2ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'r'     : [false, { 'col' : [true, '#72a4bf' ], 'bg' : [false, '#bad1de' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                's'     : [false, { 'col' : [true, '#1cb2fc' ], 'bg' : [false, '#a8e6fe' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                't'     : [false, { 'col' : [true, '#0f1e98' ], 'bg' : [false, '#838acb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'u'     : [true , { 'col' : [true, '#46b7af' ], 'bg' : [false, '#6de9ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'v'     : [false, { 'col' : [true, '#a83bd1' ], 'bg' : [false, '#ff89f1' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'w'     : [false, { 'col' : [true, '#9a4bb6' ], 'bg' : [false, '#d0a9dc' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'x'     : [false, { 'col' : [true, '#8bc227' ], 'bg' : [false, '#cbe69d' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'y'     : [false, { 'col' : [true, '#25c46f' ], 'bg' : [false, '#9be5be' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'z'     : [false, { 'col' : [true, '#1e00b4' ], 'bg' : [false, '#09b7fc' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '0'     : [true , { 'col' : [true, '#9ac900' ], 'bg' : [false, '#b9d856' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '1'     : [true , { 'col' : [true, '#008d31' ], 'bg' : [false, '#61f494' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '2'     : [true , { 'col' : [true, '#ce009e' ], 'bg' : [false, '#ef71d1' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '3'     : [true , { 'col' : [true, '#00c4d4' ], 'bg' : [false, '#77dce5' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '4'     : [true , { 'col' : [true, '#adc71d' ], 'bg' : [false, '#d1e369' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '5'     : [true , { 'col' : [true, '#e08b00' ], 'bg' : [false, '#e0b671' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '6'     : [true , { 'col' : [true, '#dd0000' ], 'bg' : [false, '#f58a8a' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '7'     : [true , { 'col' : [true, '#a409c9' ], 'bg' : [false, '#e57ffd' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '8'     : [true , { 'col' : [true, '#d82184' ], 'bg' : [false, '#ea7fb9' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '9'     : [true , { 'col' : [true, '#2100c4' ], 'bg' : [false, '#a493ff' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
            },
            'incl' : {
                'actif'   :  true ,
                'form'    : [false, { 'col' : [true , '#9c10d7' ], 'bg' : [false, '#95ae9c' ] }],
                'sep'     : [true , '·' ],
                'distrib' : [false]
            }
        };
    if (this.testmode) {
        this.opt_mots = true;
        this.syll_n = 6;
        this.opt_phon = true;
        this.diffoO = true;
        this.opt_chif = {'0':true,'1':true,'2':true,'3':true,'4':true,'5':true,'6':true,'7':true,'8':true,'9':true};
        this.opt_lett = {};
        for (let x in this.options.lett) {
            if ( (x.length === 1) && (x !== '_') && !(x in this.opt_chif)) { this.opt_lett[x] = true; }
        }
        this.opt_ponc = true;
        this.opt_incl = true;
    }
    else {
        this.opt_mots = this.options.gene.actif && this.options.gene.diff_lignes[0];
        this.syll_n = (this.options.syll.actif) ? this.options.syll.diff[1].nb : 0;
        this.opt_phon = this.options.phon.actif;
        if (this.opt_phon && (this.options.phon.o[0]||this.options.phon.O[0])) {
            let o = this.options.phon.o;
            let O = this.options.phon.O;
            this.diffoO = o[0]!==O[0] ||
                          o[1].col[0]!==O[1].col[0] || o[1].col[1]!==O[1].col[1] ||
                          o[1].bg[0]!==O[1].bg[0] || o[1].bg[1]!==O[1].bg[1] ||
                          o[1].s[0]!==O[1].s[0] || o[1].i[0]!==O[1].i[0] || o[1].g[0]!==O[1].g[0];
        }
        else {
            this.diffoO = false;
        }
        if (this.options.lett.actif) {
            this.opt_lett = {};
            this.opt_chif = {};
            let chiffres = {'0':true,'1':true,'2':true,'3':true,'4':true,'5':true,'6':true,'7':true,'8':true,'9':true};
            let o = this.options.lett;
            let no_lett = true, no_chif = true;
            for (let x in o) {
                if ( (x.length === 1) && o[x][0] && (x !== '_') ) {
                    if (x in chiffres) {
                        no_chif = false;
                        this.opt_chif[x] = true;
                    }
                    else {
                        no_lett = false;
                        this.opt_lett[x] = true;
                    }
                }
            }
            if (no_lett) { this.opt_lett = false; }
            if (no_chif) { this.opt_chif = false; }
            this.opt_ponc = o._[0];
        }
        else {
            this.opt_lett = false;
            this.opt_ponc = false;
        }
        this.opt_incl = this.options.incl.actif;
        this.opt_rien = !( this.opt_mots || this.syll_n>0 || this.opt_phon || this.opt_lett || this.opt_ponc || this.opt_incl );
    }
    this.syll_k = 1;
    this.ligne_offset = null;
    this.ligne_k = 1;
    let s = this.options.incl.sep[1];
    if (s.length === 1) {
        if (s === ' ') { s = ''; }
        if (s === '-') { s = String.fromCharCode(8209); }
        this.incl_sep = [s,'',s,s];
    }
    else {
        s = [s[0],s[1]];
        for (let i of [0,1]) {
            if (s[i] === ' ') { s[i] = ''; }
            if (s[i] === '-') { s[i] = String.fromCharCode(8209); }
        }
        this.incl_sep = [s[0],s[1],s[0],s[1]];
    }
    this.syll_sep = this.options.syll.sep[1];
    if (this.syll_sep === '-') { this.syll_sep = String.fromCharCode(8209); }
};

/* Met en place l'automate pour la lecture des phonèmes */
dysaide_Engine.prototype.automate = function() {
    if (!( this.testmode || this.opt_phon || this.syll_n>0 )) { return; }
    // Règles d'extraction des phonèmes
	/*
	 * '*' signifie 'suivi par n'importe quelle lettre
	 * '@' signifie 'dernière lettre du mot
	 *
	 * format de l'automate:
	 *		'lettre': [[règles l'ordre où elles doivent être déclenchées],[liste des règles]]
	 *
	 * 	ATTENTION. Il faut faire attention à l'ordre de précédence des règles. Plusieurs règles peuvent
	 *	en effet s'appliquer pour une même succession de lettres. Il faut ranger les règles de la plus
	 *	spécifique à la plus générale.
	 *
	 * format d'une règle :
	 *		'nom_de_la_regle': [motif, phoneme, pas]
	 *
	 *	motif : il s'agit d'une expression régulière qui sert à tester les successions de lettres qui suivent
	 *		la lettre en cours de traitement dans le mot et les successions de lettres qui précèdent la lettre
	 *		en cours de traitement.
	 *	phoneme : le nom du phonème codé selon le format ascii décrit dans
	 *		http://www.icp.inpg.fr/ICP/avtts/phon.fr.html
	 *	pas : le nombre de lettres à lire à partir de la lettre courante si le motif a été reconnu
	 *		dans le mot de part et d'autre de la lettre en cours de traitement.
	 */
	this.autom = {
		'a' : [['nm','ai_fin','in','i','u','y','*'],
               {
                '*':[{},'a',1],
                'nm':[{'+':/^(n([bcçdfgjklmpqrstvwxz]|$)|m[bp])/i},'A',2],
				'ai_fin':[{'+':/^i$/i,'!=':/^(vr|m|qu|g|ess|dél|bal|r(|embl)|miner|b|l|ge|ch|fr|papeg|br)ai$/i},'e_comp',2],
				'in':[{'+':/^i[nm]([bcçdfghjklpqrstvwxz]|$)/i},'5',3], // toute succession 'ain' 'aim' suivie d'une consonne ou d'une fin de mot
				'i':[{'+':/^[iî]/i,'!+':/^il($|l)/i},'E_comp',2],
				'u':[{'+':/^u/i},'o_comp',2],
				'y':[{'+':/^y/i,'!=':/^(b|cob|cip)aye/i},'E_comp',1]
                }],
		'â' : [['*'],
               {'*':[{},'a',1]}],
		'à' : [['*'],
			   {'*':[{},'a',1]}],
		'b' : [['b','plomb','*'],
			   {
                '*':[{},'b',1],
                'b':[{'+':/^b/i},'b',2],
				'plomb':[{'=':/(|ap?|sur)plomb($|s)/i},'H',2] // le "b" à la fin de plomb ne se prononce pas
                }],
		'c' : [['eiy','cisole','ch_k','psycho','brachio','h','c_muet_fin','ct_muet_fin','cc','*'],
			   {
                '*':[{},'k',1], //'@':['','k',1],
				'eiy':[{'+':/^[eiyéèêëîï]/i},'s_c',1],
				'cisole':[{'+':/^$/i,'-':/^$/i},'s_c',1], // exemple : c'est
				'ch_k':[{'+':/^h(r|oe|œ|or|[ée]o|estr|iro[pm]|l(o|am))/i},'k',2], // choeu, chor, chéo, orchestre, chiro, chlo, chlam, chr
				'psycho':[{'-':/psy$/i,'+':/^ho/i},'k',2], // tous les "psycho" quelque chose
				'brachio':[{'-':/bra$/i,'+':/^hio/i},'k',2], // brachiosaure, brachiocéphale
				'h':[{'+':/^h/i},'S',2],
				'c_muet_fin':[{'-':/(n|[eo]r|taba|accro)$/i,'+':/^s?$/i,'!=':/^d?onc$/i},'H',2], // clerc, porc, tabac, accroc
				'ct_muet_fin':[{'-':/(spe|in)$/i,'+':/^t(s?)$/i},'H',3],
				'cc':[{'+':/^c(?![eiyéèêëîï])/i},'k',2] // accorder, accompagner
                }],
		'ç' : [['*'],
               {'*':[{},'s',1]}],
		'd' : [['muet','d','*'],
			   {
                '*':[{},'d',1],
                'muet':[{'+':/^s?$/i,'!=':/(^d|^aujourd|^sud|(k|^)end|led|(^c?|m)a[iï]d|had|^kid|e[ei]d|ound|o[ïiu]?d|^stand|[fl]ord|rai?d|^hard|had|ld|board|^pl?ai?d|^f[ij]ord|^oued|head|band)s?$/i},'H',2], // un d final suivi éventuellement d'un s ex. : standard
				'd':[{'+':/^d/i},'d',2],
                }],
		'e' : [['jtcnslemede','tclesmesdes','caduc','eau','muet','ein','_ent','ment','verbe_3p_cons','verbe_3_pluriel',
                'ien_an','hacienda','ien','except_en','nm','hier','thriller','drz_final',
                'avoir','eu_9','eu_2','et','est','t_final','adv_emment','en_deb','re_',
                'femme','clef','2cons','y','iy','en_deb_','e_deb','ew','ew_','ee','*'],
			   {
				'*':[{},'q',1], //'@':['','q_caduc',1],
				'jtcnslemede':[{'+=':/^$/i,'=':/^([ldcsnjmt]e|que(|lques?))$/i},'q',1], // je, te, me, le, se, de, ne, que, quelque(s)
				'tclesmesdes':[{'=':/^(les($|q)|[dcsmt]es$)/i},'e_comp', 2], // mes, tes, ces, ses, les
                'caduc':[{'+':/^s?$/i,'-':/([bcçdfghjklmnpqrstvwxzy]|[qg]u)$/i},'h',1,'H',1], // un e suivi éventuellement d'un 's' et précédé d'une consonne ex. : correctes   // 'q_caduc'
                'muet':[{'+':/^(s?$|[ao])/i,'!-':/^$/i},'H',2], // les autres fins de mots et les 'e' suivis de 'a' ou 'o' sont muets
				'eau':[{'+':/^au/i},'o_comp',3],
				'_ent':[this.regle_mots_ent,'A',2], // quelques mots (adverbes ou noms) terminés par ent
				'ment':[this.regle_ment,'A',2], // on considère que les mots terminés par 'ment' se prononcent [a~] sauf s'il s'agit d'un verbe
                'verbe_3p_cons':[{'+':/^nt$/i,'-':/[bcdfghklmnprstvxz]$/i},'h',1,'H',2], // normalement, pratiquement tout le temps verbe à la 3eme personne du pluriel 
				'verbe_3_pluriel':[{'+':/^nt$/i},'H',3], // normalement, pratiquement tout le temps verbe à la 3eme personne du pluriel
                'ien_an':[this.regle_ien_an,'A',2], // ien se lisant [a~]
                'hacienda':[{'=':/hacienda/i},'E_comp',1,'n',1,'d',1,'a',1], // hacienda
                'ien':[{'-':/[iï]$/i,'+':/^n(?!n)/i},'5',2], // ien
				'except_en':[{'-':/(é|exam|mino|^édu)$/i,'+':/^ns?$/i},'5',2,'H',1], // exceptions des mots où le 'en' final se prononce [e~] (héritage latin)
				'nm':[{'+':/^[nm]([bcçdfghjklpqrstvwxz]|$)/i},'A',2],
				'hier':[this.regle_er,'E_comp',1], // encore des exceptions avec les mots terminés par 'er' prononcés 'R'
                'thriller':[this.regle_er_x,'9_comp',1], // encore des exceptions avec les mots terminés par 'er' prononcés 'eur'
				'drz_final':[{'+':/^[drz]s?$/i,'!=':/^(fez|merguez|bled|oued)$/i},'e_comp',2], // e suivi d'un d,r ou z en fin de mot done le son [e]
				'avoir':[{'-':/^$/i,'+':/^[uû](([mt]?)(e?)(s?)|rent|ss(e(s?)|ions|iez|ent))$/i},'H',1,'y',1],
                'eu_9':[{'+':/^u(r($|s)|ple(|s|nt)|[nlbfiv])/i,'!=':/(^(|m(es|on)ssi|bl|déj)eu|^meun|(gu|^m)eul|^veule(|rie)s?$)/i},'9',2],
                'eu_2':[{'+':/^[uû]/i},'2',2],
				'et':[{'=':/^et$/i},'e_comp',2],
				'est':[{'=':/^est$/i},'E_comp',3],
				't_final':[{'+':/^ts?$/i,'!=':/^([cnj]|bask|int(er|ra)n|s(|ovi)|rack|gadg|(|pick)pock|ni|velv|offs|jack|res)et/i},'E_comp',2], // donne le son [e^] et le t ne se prononce pas
                'adv_emment':[{'-':/emm$/i,'+':/^nt$/i},'A',2], // adverbe avec 'emment' => se termine par le son [a~]
				'en_deb':[{'-':/^$/i,'+':/^(n[naio]|mm)/i,'!=':/^e(nnemi|mmenth?al)/i},'A',2], // 'enn' en début de mot se prononce 'en'
				're_':[{'-':/^r$/i,'!=':/^re(ct|dd|flex|nn|pt|s([cps]|t(?!ru)))/i},'q',1], // re-quelque chose : le e se prononce 'e'
				'femme':[{'-':/f$/i,'+':/^mm/i},'a',1], // femme et ses dérivés => son [a] 
				'clef':[{'-':/cl$/i,'+':/^fs!/i},'e_comp',1,'H',2], // une clef
				'2cons':[{'+':/^([bcçdfgjklmnpqrstvxz]($|[bcçdfgjklmnpqrstvwxz])|il)/i,'!=':/^desso?us$/i},'E_comp',1], // donne le son [e^] et le l ou le c se prononcent (ex. : miel, sec)
				'ein':[{'+':/^i[nm]([bcçdfghjklpqrstvwxz]|$)/i},'5',3], // toute succession 'ein' 'eim' suivie d'une consonne ou d'une fin de mot
				'y':[{'+':/^y[aeiouéèêààäôâœ]/i},'E_comp',1],
				'iy':[{'+':/^[iy]/i},'E_comp',2],
				'en_deb_':[{'-':/^$/i,'+':/^n[aio]/i,'!=':/^e(nnemi|mmenth?al)/i},'A',1], // 'enn' en début de mot se prononce 'en'
				'e_deb':[{'-':/^$/i},'e',1], // par défaut, un 'e' en début de mot se prononce [e] (vrai, et gère les é majuscules)
                'ew':[{'+':/^ws?$/i},'u',2],
                'ew_':[{'+':/^w/i},'u',1],
                'ee':[{'+':/^e/i},'i',2]
                }],
		'é' : [['*'],
			   {'*':[{},'e',1]}],
		'è' : [['*'],
               {'*':[{},'E',1]}],
		'ê' : [['*'],
               {'*':[{},'E',1]}],
		'ë' : [['*'],
               {'*':[{},'E',1]}],
		'f' : [['f','muet','*'],
               {
                '*':[{},'f',1],
                'f':[{'+':/^f/i},'f',2],
				'muet':[{'=':/^b?(oe|œ)ufs$/i},'H',2] // oeufs et boeufs
                }],
		'g' : [['n','eiy','u','g','muet','*'],
               {
                '*':[{},'g',1],
				'n':[{'+':/^n/i,'!=':/^(dia|st|gno(s|m|u(?!f))|magnum|agnos|agnu|wa|cogni|recogni|précogni)/i},'N',2],
				'eiy':[{'+':/^[eéèêëïiy]/i},'Z_g',1], // un 'g' suivi de e,i,y se prononce [z^]
                'g':[{'+':/^g/i},'g',2],
				'u':[{'+':/^u/i,'!+':/^u([bcçdfghjklmnpqrstvwxz]|$)/i,'!=':/^aiguil/i},'g_u',2],
                'muet':[{'=':/^(lon|san|vin|poin|ran|faubour|étan|bour|haren|s(c?)hamp(o?)oin|coin|doi|parpain|amy|oran|oblon|outan|contresein|sein|oin)g($|s|t)/i},'H',2] // 'g' muets
                }],
		'h' : [['*'],
               {'*':[{},'H',1]}],
		'i' : [['gen','ient','prec_2cons','ill_j','il_j','i_voyelle','ing','ing_','nm','*'],
               {
                '*':[{},'i',1],
                'gen':[{'+':/^[bcdfgpqrstv]/i},'i',1],  // évite des tests souvent inutiles
				'ient':[this.regle_ient,'i',1,'H',3], // règle spécifique pour différencier les verbes du premier groupe 3ème pers pluriel
                'prec_2cons':[{'-':/[ptkcbdgfv][lr]$/i},'i',1], // précédé de 2 consonnes (en position 3), doit apparaître comme [ij]
				'ill_j':[{'-':/[aeoœâ]u?$/i,'+':/^ll/i},'j',3], // ill donnant [j]
				'il_j':[{'-':/[aeoœâ]u?$/i,'+':/^ls?$/i},'j',2,'H',1], // ill donnant [j]
				'i_voyelle':[{'+':/^([aäâéèêëoôöuù]|e(?!(s?)$))/i},'j',1], // i suivi d'une voyelle donne [j]
                'ing':[{'-':/[bcdfghjklmnpqrstvwxz]$/i,'+':/^ngs?$/i},'i',1,'N',2,'H',1],
                'ing_':[{'-':/^(sw|b|gr)$/i,'+':/^ng/i},'i',1,'N',2],
                'nm':[{'+':/^[nm]([bcçdfghjklpqrstvwxz]|$)/i},'5',2]
                }],
		'ï' : [['thai', 'aie','*'],
               {
                '*':[{},'i',1],
                'thai':[{'-':/^th?a$/i},'j',1], // taï, thaï et dérivés
                'aie':[{'-':/[ao]$/i,'+':/^e/i},'j',1] // païen et autres
                }],
		'î' : [['*'],
               {'*':[{},'i',1]}],
		'j' : [['*'],
               {'*':[{},'Z',1]}],
		'k' : [['*'],
               {'*':[{},'k',1]}],
		'l' : [['ill','ll','muet','*'],
               {
                '*':[{},'l',1],
				'ill':[{'-':/i$/i,'+':/^l/i,'!-':/^(|v|m|tranqu)i$/i},'j',2], // par défaut, 'ill' donne le son [j]
				'll':[{'+':/^l/i},'l',2], // à défaut de l'application d'une autre règle, 'll' donne le son [l]
                'muet':[{'=':/^(fils|cu(|cu)ls?|gentils?|fusils?|outils?|s(aou|oû)ls?|pouls|auln|coutils?)$/i},'H',2] // l muets
                }],
		'm' : [['m','tomndamn','*'],
               {
                '*':[{},'m',1],
                'm':[{'+':/^m/i},'m',2],
				'tomndamn':[{'=':/^((|con)da|auto)mn/i,'+':/^n/i},'H',1] // 'damné', 'automne'
				}],
		'n' : [['n','*'],
               {
                '*':[{},'n',1],
                'n':[{'+':/^n/i},'n',2],
               }],
		'o' : [['gen','monsieur','nm','u','in','oignon','i','oe_2','oe_9','oe_2_','oe_9_','oe_wa','oe_e','y','w','oo_o','oo_oo','oo_w','oo_u','*'],
               {
                '*':[{},'o',1],
                'gen':[{'+':/^([bcçdfgjklpqrstvwxz]|$)/i},'o',1], // évite des tests superflus dans la plupart des cas
				'monsieur':[{'=':/monsieur/i},'2',2],
				'nm':[{'+':/^[nm]([bcçdfgjklpqrstvwxz]|$)/i},'0',2],
				'u':[{'+':/^[uûù]/i},'u',2], // son [u] : clou
                'in':[{'+':/^i[nm]([bcçdfghjklpqrstvwxz]|$)/i},'w',1,'5',2],
				'oignon':[{'=':/^oignon/i},'o',1,'H',1,'N',2,'0',2],
				'i':[{'+':/^[iî]/i},'3',2],
                'oe_2':[{'+':/^eu(fs|d|x|$)/i},'2',3],
                'oe_9':[{'+':/^eu(r|f$|v)/i},'9',3],
                'oe_2_':[{'+':/^e(d|s(oph|tr)|cum|no|hn)/i},'2',2],
                'oe_9_':[{'+':/^e(i|r$)/i},'9',2],
                'oe_wa':[{'+':/^(ê|el[el])/i},'3',2],
                'oe_e':[{'+':/^e(t|nix|l[ia]|rs)/i},'e',2],
				'y':[{'+':/^y/i,'!=':/^(boy(?!a)|[gyc]oy|joys)/i},'3',1],
                'oo_o':[{'+':/^o/i,'-':/(specul|tand|alc)$/i},'o',2],
                'oo_oo':[{'+':/^o/i,'-':/[cz]$/i,'!=':/(coo[lk]|scoo|zoo(k|m(|e|a|s|ons)))/i},'o',1,'o',1],
                'oo_w':[{'+':/^o/i,'-':/(sc?hamp)$/i},'w',2,'5',2,'H',2],
                'oo_u':[{'+':/^o/i},'u',2],
                'w':[{'=':/^(clown|bowling)/i},'u',2]
                }],
		'œ' : [['oe_2','oe_9','oe_2_','oe_9_','oe_e','*'],
               {
                '*':[{'+':/^u/i},'2',2],
                'oe_2':[{'+':/^u(fs|d|x|$)/i},'2',2],
                'oe_2_':[{'+':/^(d|s(oph|tr)|cum|no|hn)/i},'2',1],
                'oe_9':[{'+':/^u(r|f$|v)/i},'9',2],
                'oe_9_':[{'+':/^(i|r$)/i},'9',2],
                'oe_e':[{'+':/^(t|nix|l[ia]|ors)/i},'e',2]
                }],
		'ô' : [['*'],
				{'*':[{},'o',1]}],
		'ö' : [['*'],
				{'*':[{},'2',1]}],
		'p' : [['gen','muet','muet_','h','p','*'],
               {
                '*':[{},'p',1],
                'gen':[{'!+':/^([spth]|$)/i},'p',1], // évite des tests superflus dans la plupart des cas
				'h':[{'+':/^h/i},'f_ph',2],
                'p':[{'+':/^p/i},'p',2],
                'muet':[{'=':/^(t(emps|rop)|coup($|s)|beauc|corps|longt|camp($|s)|champ(s|$)|drap(s|$)|loup($|s)|galop($|s)|sirop($|s)|interromp[st]|romp[st]|contretemps|contrecoup($|s)|exempt($|s)|anticorps|salop($|s)|corromp[st]|justaucorps|contrechamp($|s)|cantaloup($|s))/i},'H',2],
                'muet_':[{'=':/^(compt|sculpt|bapt|escompt|acompt|recompt|décompt|rebapt|mécompt|anabapt|débapt)/i},'H',1]
                }],
		'q' : [['kw','qu','k','kw','*'],
               {
                '*':[{},'k',1],
                'gen':[{'+':/^ue/i,'!=':/^quetsch/},'k',2], // évite des tests inutiles (67% des cas)
                'qu':[{'+':/^u[bcçdfgjklmnpqrstvwxz]/i},'k',1],
				'k':[{'+':/^u/i},'k_qu',2],
				'kw':[{'=':/^([sa]qua|[ée]qua[nt]|quadr(?!ill)|adéqua|qua(tuor|rtz)|qu(o$|ak)|inadéqua|quant(a|um|i$)|qu(etsch|a($|ter|rk))|kumq|péréqua|desquam|subaqua|antiquark)/i,'!+':/^ues?$/i},'k',1,'w',1]
                }],
		'r' : [['r','muet','*'],
               {
                '*':[{},'r',1],
				'r':[{'+':/^r/i},'r',2],
                'muet':[{'=':/^(m(on|es)sieu|ga)rs$/i},'H',2]
                }],
		's' : [['s_final','muet','para','z','s','sch','h','*'],
               {
                '*':[{},'s',1], //'@':[{},'H',1],
				's_final':[this.regle_s_final,'s',1], // quelques mots terminés par -us, -is, -os, -as
				'muet':[{'+':/^$/i,'!=':/^s$/i},'H',1], // un s en fin de mot, ex. : correctes
				'para':[{'-':/para$/i,'!+':/^sit/i},'s',1], // para quelque chose (parasol, parasismique, ...), sauf parasit*
				'z':[{'-':/([aeiyouéèàüûùëöêîôœ]|^bun)$/i,'+':/^[aeiyouéèàüûùëöêîôœ]/i},'z_s',1], // un s entre 2 voyelles (ou Bunsen) se prononce [z]
                'sch':[{'+':/^ch/i},'S',3], // schlem
				'h':[{'+':/^h/i,'!-':/^dés$/i},'S',2],
				's':[{'+':/^s/i},'s',2] // un s suivi d'un autre s se prononce [s]
                }],
		't' : [['t_final','muet','t','except_tien','tieon','cratie','*'],
               {
                '*':[{},'t',1], //'@':[{},'H',1],
				't_final':[this.regle_t_final,'t',1], // quelques mots où le "t" final se prononce
				'muet':[{'+':/^s?$/i,'!=':/^t$/i},'H',2], // un t suivi éventuellement d'un s ex. : marrants
                't':[{'+':/^t/i},'t',2],
				'except_tien':[this.regle_tien,'t',1], // quelques mots où 'tien' se prononce [t]
				'tieon':[{'+':/^i[eo]n/i},'s_t',1,'j',1],
				'cratie':[{'-':/cra$/i,'+':/^ie/i},'s_t',1]
                }],
		'u' : [['un','ueil','u','n_9','n_unct','n_u','n_on','um','nm','*'],
               {
                '*':[{},'y',1],
                'un':[{'=':/^un$/i},'1',2], // Evite des tests nombreux et le plus souvent inutile
				'ueil':[{'+':/^eil/i},'9',2,'j',2], // mots terminés en 'ueil' => son [x]
                'u':[{'!+':/^([nm](?![aeiouyâéèîônœ])|mn)/i},'y',1], // Evite des tests nombreux et le plus souvent inutile
                'n_9':[{'+':/^n(ch|k|ge|dee)/i,'!=':/^((b|d|sk)unk|nunch)/i},'9',1,'n',1],
                'n_unct':[{'+':/^nct/i},'0',2,'k',1,'t',1],
                'n_u':[{'=':/^(bunk|[kjm]ung|nunch)/i,'+':/^n/i},'u',1,'n',1],
                'n_on':[{'+':/^n/i,'-':/^(s(ec|k)|av|hom)$/i},'0',2],
                'um':[{'+':/^m($|n|s)/i,'!=':/^parf/i},'o',1],  // O   (mn : médium+, parf: composés de parfum+ déjà traités)
				'nm':[{},'1',2]   // Plus de test à faire grâce au filtre 'u'
                }],
		'û' : [['*'],
				{'*':[{},'y',1]}],
		'ù' : [['*'],
				{'*':[{},'y',1]}],
		'v' : [['*'],
				{'*':[{},'v',1]}],
		'w' : [['w_v','wa','*'],
               {
                '*':[{},'w',1],
                'w_v':[{'=':/^(wag|interview|edelweiss|w(isigo|alkyr|estphal))/i},'v',1],
                'wa':[{'+':/^a/i},'3',2] // watt, wapiti, etc.
                }],
		'x' : [['x_s','muet','muet_','gz','*'],
               {
                '*':[{},'x',1], //'@':[{},'H',1],
                'x_s':[{'=':/^([sd]i|coccy)x$/i},'s_x',1], // six, dix, coccys
                'muet':[{'+':/^$/i,'-':/(u|(o|a|pr)i)$/i,'!=':/^aix$/i,'!-':/lu$/i},'H',1],  // presque toutes les terminaisons en 'x' muettes
                'muet_':[{'=':/^(crucifi|perdri)x$/i},'H',1],  // deux exceptions ignorées par la règles "muet" (pour ne pas qu'elle couvre les dinosaures)
                'gz':[{'-':/^(|(|co|in|p?ré)e)$/i,'+':/^[aeéèhiouy]/i},'X',1]
                }],
		'y' : [['abbaye','y_voyelle','nm','*'],
               {
                '*':[{},'i',1],
                'abbaye':[{'=':/^abbaye/i},'i',1,'H',2], // Pas traité comme exception pour éviter de devoir le faire aussi pour 'e'.
                'y_voyelle':[{'+':/^[aeiouéèàüëöêîôûù]/i},'j',1], // y suivi d'une voyelle donne [j]
                'nm':[{'+':/^[nm][bcçdfghjklpqrstvwxz]/i},'5',2]
                }],
		'z' : [['riz','tz','*'],
               {
                '*':[{},'z',1],
                'riz':[{'=':/^r[ia]z$/i},'H',1],
                'tz':[{'-':/t$/i},'s',1]
                }]
	};
	// Quelques vérifications de bases sur la validité de this.autom
    /*
    for (let lettre in this.autom) {
        let ordre = this.autom[lettre][0];
        if (ordre[ordre.length-1] !== '*') { console.error("Lettre "+lettre+" : la dernière règle n''est pas ''*''."); }
        let regles = this.autom[lettre][1];
        for (let regle of ordre) {
            if (!(regle in regles)) { console.error('Lettre '+lettre+' : regle '+regle+' non definie.'); }
        }
        for (let regle in regles) {
            let cle = regles[regle][0];
            if (!(regle in regles)) { console.warn('Lettre '+lettre+' : regle '+regle+' definie mais pas utilisee.'); }
            if (cle && typeof(cle) !== "function") {
                for (let c in cle) {
                    if ( /^\!?[=+-]$/i.exec() ) {
                        console.warn('Lettre '+lettre+', regle '+regle+' :  Tag '+c+' non reconnu.');
                    }
                    if ( cle[c].constructor !== RegExp ) {
                        console.error('Lettre '+lettre+', regle '+regle+' :  '+cle[c]+" n'est pas une expression reguliere.");
                    }
                }
                for (let c of ['-','!-']) {
                    if ( (c in cle) && ( cle[c].source[cle[c].source.length-1].slice(-1) !== '$' ) ) {
                        console.warn('Lettre '+lettre+', regle '+regle+' :  $ manquant en '+c+' : '+cle[c]);
                    }
                }
                for (let c of ['+','!+']) {
                    if ( (c in cle) && ( cle[c].source[0] !== '^' ) ) {
                        console.warn('Lettre '+lettre+', regle '+regle+' :  ^ manquant en '+c+' : '+cle[c]);
                    }
                }
            }
            if ( (regles[regle].length < 3) || (regles[regle].length % 2 === 0) ) {
                console.error('Lettre '+lettre+', regle '+regle+' : format du retour incorrect.');
            }
            for (let i = 1; i < regles[regle].length; i += 2) {
                if ( ( typeof(regles[regle][i]) !== 'string' ) || ( typeof(regles[regle][i+1]) !== 'number' ) ) {
                    console.error('Lettre '+lettre+', regle '+regle+' : format du retour incorrect.');
                }
                if ('pbtdkgfvszSZmnxXNGlrwjieEaOoy29qu3A015hH-I'.indexOf(regles[regle][i][0])<0 || regles[regle][i][0].length !== 1 ) {
                    console.error('Lettre '+lettre+', regle '+regle+' : produit la phoneme non-listee '+regles[regle][i][0]+'.');
                }
            }
        }
    }
    */
    // Reformatage
    for (let lettre in this.autom) {
        let regles = [];
        for (let regle of this.autom[lettre][0]) {
            let lit = [], r = this.autom[lettre][1][regle];
            for (let i = 1; i < r.length; i += 2) {
                let ph = r[i][0];
                if (ph === 'h') {
                    if (this.options.phon.q_fin[0]) {
                        ph = 'q';
                    }
                    else
                    if (!(this.testmode || this.options.syll.actif) || this.options.syll.oral[0]) {
                        lit = false;
                        break;
                    }
                }
                lit.push( [ ph , r[i+1] ] );
            }
            if (lit) { regles.push([r[0],lit]); }
        }
        this.autom[lettre] = regles;
    }
    /* Pour une lettre L et une règle R, this.autom[L][1][R] est [ règles , lecture ],
       où 'règles' est la tableau des conditions ('=', '-', '+', '!=', '!-', '!+'),
       et 'lecture' est la table des [phonèmes,taille] (par exemple : [[i,1],[H,3]]). */
    let voy = {'i':true,'e':true,'E':true,'a':true,'O':true,'o':true,'y':true,'2':true,
               '9':true,'q':true,'u':true,'3':true,'A':true,'0':true,'1':true,'5':true};
    if (!this.options.syll.oral[0]) { voy.h = true; }
    if (this.testmode) {
        this.voyelles = voy;
        this.est_voyelle = function(phoneme) { return (phoneme.phoneme in this.voyelles); };
    }
    else {
        if (this.options.phon.actif) {
            if (this.syll_n>0 || this.diffoO) {
                for (let lettre in this.autom) {
                    for (let regle of this.autom[lettre]) {
                        let res = [], ph_ = null;
                        for (let phoneme of regle[1]) {
                            let ph = phoneme[0];
                            if ( (ph==='h') ? !this.options.phon.H[0] : !this.options.phon[ph][0] ) {
                                if (ph in voy) {
                                    ph = '+';
                                }
                                else {
                                    ph = ' ';
                                }
                                if (ph === ph_) {
                                    res[res.length-1][1] += phoneme[1];
                                    continue;
                                }
                            }
                            res.push([ph,phoneme[1]]);
                            ph_ = ph;
                        }
                        regle[1] = res;
                    }
                }
                for (let ph in voy) {
                    if ( (ph==='h') ? !this.options.phon.H[0] : !this.options.phon[ph][0] ) {
                        delete voy[ph];
                        voy['+'] = true;
                    }
                }
                this.voyelles = voy;
                this.est_voyelle = function(phoneme) { return (phoneme.phoneme in this.voyelles); };
            }
            else {
                for (let lettre in this.autom) {
                    for (let regle of this.autom[lettre]) {
                        let res = [], ph_ = null;
                        for (let phoneme of regle[1]) {
                            let ph = phoneme[0];
                            if (!this.options.phon[ph][0]) {
                                ph = ' ';
                                if (ph === ph_) {
                                    res[res.length-1][1] += phoneme[1];
                                    continue;
                                }
                            }
                            res.push([ph,phoneme[1]]);
                            ph_ = ph;
                        }
                        regle[1] = res;
                    }
                }
            }
        }
        else {
            for (let lettre in this.autom) {
                for (let regle of this.autom[lettre]) {
                    let res = [], ph_ = null;
                    for (let phoneme of regle[1]) {
                        let ph = (phoneme[0] in voy) ? '+' : ' ';
                        if (ph === ph_) {
                            res[res.length-1][1] += phoneme[1];
                            continue;
                        }
                        res.push([ph,phoneme[1]]);
                        ph_ = ph;
                    }
                    regle[1] = res;
                }
            }
            this.est_voyelle = function(phoneme) { return phoneme.phoneme === '+'; };
        }
    }
};


/***********************************
*  Règles de lectures de phonèmes  *
***********************************/

/* Règle spécifique de traitement des successions de lettres finales 'ient'
 * sert à savoir si la séquence 'ient' se prononce [i][H] ou [j][e~].
 * True => [i][H]
 */
dysaide_Engine.prototype.regle_ient = function(mot, pos_mot, taille_mot) {
    // On doit être à la fin du mot, il doit être assez long, et se terminer par 'ient'.
    if ( ( taille_mot < 5 ) || (pos_mot+4 < taille_mot) || (mot.slice(-4) !== 'ient') ) { return false; }
    // Corps de verbes en 'ier' sans accent, + 'r' (rient) + 'sour' ("sourient")
    switch (mot[taille_mot-5]) {
        case 'a':
        case 'o':
            return true;
        case 'v':
            return ( ['','de','re','con','sou','par','re','inter','pro','rede','pré','sur','ad','contre','sub','circon'].indexOf(mot.slice(0,-5)) < 0 );
        case 'c':
            return ( ['cons','incons','incons','subcons','coeffi','es','cons','omnis','défi','subcons','effi','défi'].indexOf(mot.slice(0,-5)) < 0 );
        case 't':
            return ( ['','appar','pa','re','con','pa','impa','ob','sou','main','dé','entre','quo','impa','abs'].indexOf(mot.slice(0,-5)) < 0 );
        case 'd':
            return ( ['ingré','expé','expé','gra'].indexOf(mot.slice(0,-5)) < 0 );
        case 'l':
            return ( ['c','émol'].indexOf(mot.slice(0,-5)) < 0 );
        case 'n':
            return ( mot.slice(0,-5) != 'inconvé' );
        case 'p':
            return ( mot.slice(0,-5) != 'réci' );
        case 'r':
            return ( mot.slice(0,-5) != 'o' );
    }
    return true;
};

/* Règle spécifique de traitement des successions de lettres 'ien'
 * sert à savoir si le en se prononcent a~.
 * True => [a~]
 */
dysaide_Engine.prototype.regle_ien_an = function(mot, pos_mot, taille_mot) {
    // On vérifie qu'on a bien "ien."
    if ( (pos_mot == 1) || (pos_mot > taille_mot-2) ||
         (mot[pos_mot] != 'n') ||
         ( (mot[pos_mot-2] != 'i') && (mot[pos_mot-2] != 'ï') )  ) { return false; }
    // Selon la lettre suivant le n
    switch (mot[pos_mot+1]) {
        case 'c':
        case 'l':
            return true;
        case 't':
            // Seul quelques mots avec 'ient' se lisent e~
            return ( ['chrétienté','appartient','détient','obtient','contient',
                      'soutient','tient','maintient','entretient','abstient','retient'].indexOf(mot) < 0 );
    }
    return false;
};

/* Règle spécifique de traitement des successions de lettres '*ent'
 * sert à savoir si le mot figure dans les mots qui se prononcent a~ à la fin.
 * True => [a~]
 */
dysaide_Engine.prototype.regle_mots_ent = function(mot, pos_mot, taille_mot) {
    // Un mot se terminant par 'ents' est en a~
    if ( mot.slice(-4) === 'ents' ) { return (pos_mot+3 === taille_mot); }
    // On doit en être à la fin du mot, et ce dernier doit se finir par 'ent'.
	if ( (pos_mot+2 < taille_mot) || ( mot.slice(-3) !== 'ent') ) { return false; }
    // Un mot de 4 lettres (finissant par 'ent') est en 'a~'.
    if ( taille_mot == 4 ) { return true; }
	// Ensemble de mots qui se terminent par -ent, sans accent
    // cent, dent, gent, lent, vent ôtés : déjà traité par la règle des 4 lettres
    switch (mot[taille_mot-4]) {
        case 'c':
            return ( ['','ac','inno','adoles','ré','dé','indé','convales','réti','incandes','phosphores','évanes','evanes','fluores','efferves','pour','lumines','adja','opales','concupis','putres','turges','déliques','lactes','spumes','arbores','irides','tumes'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'd':
            return ( ['prési','acci','évi','evi','inci','pru','','précé','ar','impru','confi','stri','chien','occi','rési','excé','déca','impu','dissi','tri','antécé','a','coïnci'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'g':
            return ( ['ar','a','intelli','ser','ur','indul','négli','contin','ré','','déter','diver','indi','entre','émer','emer','dili','conver','astrin','inintelli','tan'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'i':
            return ( ['cl','pat','impat','inconsc','consc','inconvén','or','récip','subconsc','coeffic','ingréd','esc','expéd','quot','défic','omnisc','effic','émoll','emoll','grad'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'l':
            return ( ['ta','excel','','vio','équiva','equiva','inso','re','turbu','succu','somno','corpu','indo','opu','sanguino','do','viru','trucu','puru','ambiva','polyva','flatu','fécu','cova'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'n':
            return ( ['conti','perma','immi','émi','emi','imperti','inconti','perti','proémi','imma','absti','réma'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'p':
            return ( ['ser','re','ar'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'r':
            return ( ['diffé','indiffé','pa','transpa','tor','appa','concur','cohé','incohé','défé','inhé','récur','adhé','affé','réfé','interfé'].indexOf(mot.slice(0,-4)) >= 0 );
        case 's':
            return ( ['pré','','ab','res','pres','con','omnipré'].indexOf(mot.slice(0,-4)) >= 0 );
        case 't':
            return ( ['con','mécon','compé','incompé','impo','péni','intermit','la','ventripo','impéni','omnipo','pa','malcon'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'u':
            return ( ['conséq','fréq','éloq','eloq','infl','ong','confl','affl','grandiloq','inconséq','subséq','effl'].indexOf(mot.slice(0,-4)) >= 0 );
        case 'v':
            return ( ['sou','','cou','au','para','fer','a','engoule','é','e','con','contre'].indexOf(mot.slice(0,-4)) >= 0 );
    }
    return false;
};

/* Règle spécifique de traitement des successions de lettres 'ment'
 * sert à savoir si le mot figure dans les mots qui se prononcent a~ à la fin.
 * True => [a~]
 */
dysaide_Engine.prototype.regle_ment = function(mot, pos_mot, taille_mot) {
    // Un mot se terminant par 'ents' est en a~
    if ( mot.slice(-5) === 'ments' ) { return (pos_mot+3 === taille_mot); }
    // On doit être à la fin du mot, et il doit se terminer par 'ment'.
    if ((taille_mot < 6) || (pos_mot+3 < taille_mot) || (mot.slice(-4) !== 'ment')) { return false; }
	// Ensemble de verbes qui se terminent par -mer, sans accent
	switch (mot[taille_mot-5]) {
        case 'a':
            return ( ['récl','ent','procl','cl','excl','r','accl','aff','c','cr','tr','diff','br','d','p'].indexOf(mot.slice(0,-5)) < 0 );
        case 'h':
            return ( ['establis','ryt'].indexOf(mot.slice(0,-5)) < 0 );
        case 'i':
            return ( ['a','expr','est','an','ab','impr','r','tr','m','pr','dépr','suppr','oppr','ran','enven','escr','compr','essa','déc','arr','fr','l','gr','br','subl','légit','pér'].indexOf(mot.slice(0,-5)) < 0 );
        case 'l':
            return ( ['ca','fi'].indexOf(mot.slice(0,-5)) < 0 );
        case 'm':
            switch ( mot[taille_mot-5] ) {
                case 'a':
                    return ( /(^enfl|progr)$/i.exec(mot.slice(0,-6)) === null );
                case 'o':
                    return ( ['n','cons','ass','surn','prén','s','g','dén','dég'].indexOf(mot.slice(0,-6)) < 0 );
            }
            return true;
        case 'r':
            return ( ['do','fo','fe','affi','transfo','confi','endo','refe','enfe','info','défo','renfe','cha','a','confo','refo','ge','rendo','désa','infi'].indexOf(mot.slice(0,-5)) < 0 );
        case 's':
            return ( mot.slice(0,-5) != 'fanta' );
        case 'u':
            return ( ['f','all','emba','rés','cons','ass','parf','br','rall','embr','h','éc','ec','pa','accout','enrh','pl'].indexOf(mot.slice(0,-5)) < 0 );
        case 'â':
            return ( ['bl','p'].indexOf(mot.slice(0,-5)) < 0 );
        case 'è':
            return ( ['s','pars','blasph'].indexOf(mot.slice(0,-5)) < 0 );
        case 'î':
            return ( mot.slice(0,-5) != 'ab' );
        case 'o':
        case 'ô':
            return ( mot.slice(0,-5) != 'ch' );
    }
    return true;
};

/* Règle spécifique de traitement des successions de lettres finales 'er'
 * sert à identifier les mots lus comme "hier" ([e^], "ère")
 * True => [e^]
 */
dysaide_Engine.prototype.regle_er = function(mot, pos_mot, taille_mot) {
    // Retrait de l'éventuel 's' final
    if ( mot[taille_mot-1] == 's' ) { mot = mot.slice(0,-1); }
    // On doit être à la fin du mot, et il doit se terminer par 'er'.
    if ( (taille_mot < 3) || (pos_mot+1 < taille_mot) || (mot.slice(-2) !== 'er')) { return false; }
	// Liste des mots en -er lus "ère"
    switch (mot[taille_mot-3]) {
        case 'b':
            return ( ['','we'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'c':
            return ( mot.slice(0,-3) === 'can' );
        case 'd':
            return ( mot.slice(0,-3) === '' );
        case 'f':
            return ( ['en','entre',''].indexOf(mot.slice(0,-3)) >= 0 );
        case 'h':
            return ( ['béc','casc','cas','c','karc','kasc','kas','polyet','polyét'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'i':
            return ( ['f','h','requ','t','acqu','conqu','enqu','reconqu'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'k':
            return ( ['coc','doc','jo','po'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'l':
            return ( ['dopp','rottwei','rottweil'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'm':
            return ( ['a','kh','','outre'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'n':
            return ( ['contai','coro','scan'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'p':
            return ( ['hy','im','','su'].indexOf(mot.slice(0,-3)) >= 0 );
        case 's':
            return ( ['gey','la','res',''].indexOf(mot.slice(0,-3)) >= 0 );
        case 't':
            return ( ['al','as','as','cathé','cathe','char','ches','drags','es','hams','hélicop','helicop','in','jupi','magis','polyes','pos','sphinc','suppor','winches'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'v':
            return ( ['a','de','di','en','hi','per','pullo','re','revol','révol','tra','uni',''].indexOf(mot.slice(0,-3)) >= 0 );
        case 'z':
            return ( ['bla','bulldo'].indexOf(mot.slice(0,-3)) >= 0 );
    }
    return false;
};

/* Règle spécifique de traitement des successions de lettres finales 'er'
 * sert à identifier les mots lus comme "thriller" ([x], "eur")
 * True => [x]
 */
dysaide_Engine.prototype.regle_er_x = function(mot, pos_mot, taille_mot) {
    // Retrait de l'éventuel 's' final
    if ( mot[taille_mot-1] == 's' ) { mot = mot.slice(0,-1); }
    // On doit être à la fin du mot, et il doit se terminer par 'er'.
    if ( (taille_mot < 3) || (pos_mot+1 < taille_mot) || (mot.slice(-2) !== 'er')) { return false; }
	// Liste des mots en -er lus "eur"
    switch (mot[taille_mot-3]) {
        case 'd':
            return ( ['blen','colea','freeri','highlan','lea','snowboar','tra'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'f':
            return ( mot.slice(0,-3) == 'buf' );
        case 'g':
            return ( ['bur','challen','hambur','jog','la','teena'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'h':
            return ( mot.slice(0,-3) == 'birc' );
        case 'k':
            return ( ['bi','bookma','bun','crac','hac','mar','pacema','sha','snea','snoo','spea','stic','supertan','tan','trac'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'l':
            return ( ['boi','dea','fil','rol','spoi','thril'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'm':
            return ( ['babyboo','strea'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'n':
            return ( ['croo','desig'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'p':
            return ( ['bum','flip','pop','sni','strip'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'r':
            return ( mot.slice(0,-3) == 'fuh' );
        case 's':
            return ( ['crui','kai','lo','tea'].indexOf(mot.slice(0,-3)) >= 0 );
        case 't':
            return ( ['af','babysit','blis','blockbus','bus','car','clus','cut','ghostwri','ha','hips','mas','mis','ska','star','toas','twee','webmas','wri'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'v':
            return ( ['crosso','o','ro','turno'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'x':
            return ( ['bo','mi'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'y':
            return ( ['destro','fl'].indexOf(mot.slice(0,-3)) >= 0 );
        case 'z':
            return ( ['buz','free','pan'].indexOf(mot.slice(0,-3)) >= 0 );
        default:
            return false;
    }
    /*  Sont reconnus comme noms communs plutôt que verbes à l'infinitif :
            boxer, buzzer, designer
        Sont reconnus comme verbes à l'infinitif plutôt que noms communs :
            biper, bipper, booster, interviewer, ranger, rider, surfer
    */
};

/* Règle spécifique de traitement des mots qui se terminent par "us".
 * Pour un certain nombre de ces mots, le 's' final se prononce.
 * True => [s]
 */
dysaide_Engine.prototype.regle_s_final = function(mot, pos_mot, taille_mot) {
    // Si l'on est pas à la fin du mot, on ne perd pas de temps
    if (pos_mot < taille_mot) { return false; }
    // Liste des mots en 's, triés selon leur avant-dernière lettre
    // (ce qui permet un traitement rapide des cas les plus communs)
    let mots_s;
    switch (mot[pos_mot-2]) {
        case 's':
        case 'è':
            return true;
        case 'a':
            mots_s = ['alias','as','atlas','gambas','hélas','helas','veritas','maracas',
                      'muchas','pancréas','pancreas','plexiglas','pseudomonas','sensas',
                      'tapas','trias','vasistas'];
            break;
        case 'b':
            return ( mot == 'clebs' );
        case 'c':
            return ( mot == 'pacs' );
        case 'e':
            return ( mot == 'patres' );
        case 'i':
            mots_s = ['anis','apis','bis','cannabis','catharsis','clitoris','cochylis',
                      'éléphantiasis','eléphantiasis','elephantiasis','gratis','habilis',
                      'ibis','extremis','iris','jadis','lapis','loris','métis','metis',
                      'myosis','myosotis','oasis','orchis','pastis','pelvis','pénis',
                      'penis','phimosis','phtiriasis','pityriasis','propolis','psoriasis',
                      'ptosis','pubis','reis','synopsis','syphilis','tennis','tournevis',
                      'vis'];
            break;
        case 'ï':
            return ( mot == 'maïs' );
        case 'l':
            return ( mot == 'fils' );
        case 'm':
            return ( mot == 'williams' );
        case 'n':
            mots_s = ['afrikaans','contresens','tremens','leggins','sens','trans'];
            break;
        case 'o':
            mots_s = ['albatros','albinos','bastos','calmos','calvados','cosmos','craignos',
                      'éros','eros','gratos','intramuros','muros','laos','dios',
                      'matos','mos','os','ouroboros','pathos','pesos','rhinocéros',
                      'rhinoceros','spéculoos','speculoos','tétanos','tetanos','thanatos',
                      'thermos','tranquillos'];
            break;
        case 'p':
            mots_s = ['biceps','chips','forceps','laps','peps','princeps','quadriceps',
                      'relaps','schnaps','triceps','tricératops','triceratops'];
            break;
        case 'r':
            mots_s = ['knickers','mars','nounours','ours'];
            break;
        case 't':
            mots_s = ['boots','peanuts'];
            break;
        case 'u':
            mots_s = ['abribus','airbus','angélus','angelus','animus','antivirus','anus',
                      'argus','asparagus','aspergillus','autobus','autofocus','bibliobus',
                      'blockhaus','blocus','bonus','bus','cactus','campus','chorus',
                      'citrus','collapsus','consensus','corpus','cosinus','couscous',
                      'crésus','cresus','crocus','cubitus','cumulonimbus','nimbus','cumulus',
                      'cunnilingus','cursus','détritus','detritus','diplodocus','entérovirus',
                      'enterovirus','eucalyptus','ficus','focus','foetus','gibus','gus',
                      'hantavirus','hiatus','hibiscus','erectus','humérus','humerus','humus',
                      'hypothalamus','infarctus','laïus','laius','lapsus','locus','lotus',
                      'lupus','malus','maous','minibus','minus','mordicus','motus','mucus',
                      'nimbostratus','stratus','nimbus','nucléus','nucleus','numerus',
                      'clausus','oculus','olibrius','omnibus','opus','orémus','oribus',
                      'papillomavirus','papyrus','pédibus','pedibus','phallus','plexus',
                      'plus','processus','prolapsus','prospectus','proteus','protococcus',
                      'prunus','radius','rébus','rebus','rétrovirus','retrovirus','rhésus',
                      'rhesus','rictus','sanctus','sinus','stimulus','stradivarius','stratus',
                      'sus','syllabus','terminus','thalamus','thésaurus','thesaurus','thymus',
                      'tonus','tophus','tous','tractus','trolleybus','tumulus','typhus',
                      'ultravirus','us','utérus','uterus','vénus','venus','versus','virus'];
            break;
        case 'y':
            return ( mot == 'lys' );
    }
    return false;
};

/* Règle spécifique de traitement des mots qui se terminent par la lettre "t" prononcée.
 * True => [t]
 */
dysaide_Engine.prototype.regle_t_final = function(mot, pos_mot, taille_mot) {
    // Retrait de l'éventuel 's' final
    if ( mot[taille_mot-1] == 's' ) { mot = mot.slice(0,-1); }
    // Si l'on est pas à la fin du mot, on ne perd pas de temps
    if ( pos_mot < taille_mot ) { return false; }
    // Liste des mots en -t, triés selon leur avant-dernière lettre
    // (ce qui permet un traitement rapide des cas les plus communs)
    let mots_s;
    switch (mot[pos_mot-2]) {
        case 'f':
        case 'h':
        case 'l':
        //case 't':    // Pris en charge par la règle du double-t, qui précède
            return true;
        case 's':
            // Seul 'est' (être) a un t muet.
            return (mot!='est');
        case 'c':
            // Les mots dont on ne prononce pas le t sont en -pect ou en -inct
            if (taille_mot < 6) { return true; } // "aspect" est la plus petit mot en -ct muet
            mot = mot.slice(-4);
            return ( (mot!='pect') && (mot!='inct') );
        case 'a':
            mots_s = ['audimat','beat','fat','fiat','khat','kumquat','mat','squat','stat','transat'];
            break;
        case 'e':
            mots_s = ['basket','cet','gadget','internet','intranet','ket','net','niet','offset',
                      'pickpocket','racket','reset','rocket','set','socket','soviet','street','velvet'];
            break;
        case 'i':
            mots_s = ['accessit','akvavit','aquavit','audit','bit','cockpit','coit','digit','dixit',
                      'déficit','deficit','exit','fahrenheit','granit','hit','instit','inuit','it',
                      'kit','pschit','purit','shit','transit'];
            break;
        case 'n':
            mots_s = ['discount','establishment','event','shunt','sprint'];
            break;
        case 'o':
            mots_s = ['dot','foot','hot','shoot','spot'];
            break;
        case 'p':
            mots_s = ['abrupt','concept','rapt','script','sept','transept'];
            break;
        case 'r':
            mots_s = ['appart','flirt','kart','shirt','short','smart','yaourt','yoghourt','yogourt'];
            break;
        case 'u':
            mots_s = ['août','azimut','brut','but','chut','input','kapout','knout','mazout','occiput',
                      'out','out','prout','raout','rut','scorbut','scout','stout','uppercut','ut','zut'];
            break;
        case 'ï':
            return ( mot == 'coït' );
        case 'û':
            return ( mot == 'août' );
    }
    return false;
};

/* Règle spécifique de traitement de quelques mots qui se terminent par 'tien' et
 * dans lesquels le 't' se prononce [t]
 * True => [t]
 */
dysaide_Engine.prototype.regle_tien = function(mot, pos_mot, taille_mot) {
    // Retrait de l'éventuel 's' final
    if ( mot[taille_mot-1] == 's' ) { mot = mot.slice(0,-1); }
    // On doit en être à la fin du mot, et ce dernier doit se finir par 'tien'.
	if ( (pos_mot+3 < taille_mot) || ( mot.slice(-4) !== 'tien') ) { return false; }
    // Liste des mots en -tien donnant [t]
    let possibles = ['tien','antichrétien','antichretien','chrétien','chretien','entretien',
                     'faustien','kantien','maintien','proustien','soutien'];
    return (possibles.indexOf(mot) >= 0);
};


/***********************************
*  Règles de lectures de phonèmes  *
***********************************/

/* Décompose un mot 'mot' (string) en série de phonèmes (dysaide_Phonème). */
dysaide_Engine.prototype.lire_mot = function(mot) {
    let lu = [];
    let taille = mot.length;
    let pos = 1;
    let lc_mot = mot.toLowerCase();
    // On parcourt le mot
    while (pos <= taille) {
        let regle;
        if (lc_mot[pos-1] in this.autom) {
            let gauche = null, droite = null;
            // Règles pour la lettre en court
            let loc_autom = this.autom[lc_mot[pos-1]];
            // On parcourt ces règles jusqu'à celle qui correspond
            for (regle of loc_autom) {
                let cle = regle[0];
                if (cle === {}) { break; }
                // Si le test est une fonction, on l'appelle.
                if (typeof(cle) === "function") {
                    if ( cle(lc_mot, pos, taille) ) { break; }
                }
                // Sinon :
                else {
                    if ( ('=' in cle) && (cle['='].exec(mot) === null) ) { continue; }    
                    // On vérifie la condition '+' s'il y en a une. (En premier car plus rapide (regexp testant les premières lettres).)
                    if ('+' in cle) {
                        if (droite === null) { droite = mot.slice(pos); }
                        if (cle['+'].exec(droite) === null) { continue; }
                    }
                    // On vérifie la condition '-' s'il y en a une.
                    if ('-' in cle) {
                        if (gauche === null) { gauche = mot.substr(0,pos-1); }
                        if (cle['-'].exec(gauche) === null) { continue; }
                    }
                    // On vérifie les exceptions.
                    if ( ('!=' in cle) && (cle['!='].exec(mot) !== null) ) { continue; }
                    if ('!+' in cle) {
                        if (droite === null) { droite = mot.slice(pos); }
                        if (cle['!+'].exec(droite) !== null) { continue; }
                    }
                    if ('!-' in cle) {
                        if (gauche === null) { gauche = mot.substr(0,pos-1); }
                        if (cle['!-'].exec(gauche) !== null) { continue; }
                    }
                    // Les conditions sont respectées.
                    break;
                }
            }
        }
        else {
            regle = [null,[[' ',1]]];
        }
        // On stocke la ou les phonèmes lues
        for (let phoneme of regle[1]) {
            lu.push( new dysaide_Phoneme(phoneme[0],mot.substr(pos-1, phoneme[1])) );
            pos += phoneme[1];
            if (pos > taille) { break; }
        }
    }
    if (this.diffoO) { this.affine_o(lu); }
    return lu;
};

/* Affine les phonèmes [o] d'une lecture 'lu' (Array(dysaide_Phonème)), différenciant les o ouverts [O] des o fermés [o]. */
dysaide_Engine.prototype.affine_o = function(lu) {
    for (let i = 0; i < lu.length; i++) {
        if ( lu[i].phoneme === 'o' ) {
            if ( lu[i].lettres === 'o' ) {
                let o = lu[i];
                while ( ( ++i < lu.length ) && ( lu[i].phoneme.toUpperCase() === 'H' ) ) {} // On va à la prochaine phonème non muette
                if ( i === lu.length ) { return; }                            // Bout du mot => o fermé
                if ( lu[i].phoneme === 'r' ) { o.phoneme = 'O'; continue; }   // -or- => O ouvert
                if ( this.est_voyelle(lu[i]) ) { i--; continue; }                   // -o[voy]- => o fermé
                let c = lu[i];
                while ( ( ++i < lu.length ) && ( lu[i].phoneme.toUpperCase() === 'H' || !this.est_voyelle(lu[i]) ) ) {}    // On va à la prochaine voyelle
                if ( i === lu.length ) {                                      // Bout du mot (après consonnes) => O ouvert sauf exceptions
                    if ( c.lettres === 'm' ) {     // Mot terminé par -ome(s)
                        let mot = ''; for (let ph in lu) { mot += ph.lettres; }
                        if ( /(ron|^([tn]|[ée]con|majord))ome/i.exec(mot) !== null ) { o.phoneme = 'O'; }
                        return;
                    }
                    if ( c.lettres === 'n' ) {     // Mot terminé par -one(s)
                        let mot = ''; for (let ph in lu) { mot += ph.lettres; }
                        if ( /(z|c(l|hr)|neur)one/i.exec(mot) === null ) { o.phoneme = 'O'; }
                        return;
                    }
                    o.phoneme = 'O'; return;
                }
                if ( lu[i].phoneme === 'q' ) { o.phoneme = 'O'; continue; }   // -o[consonnes][q]- => O ouvert
                i--;
            }
            else
            if ( lu[i].lettres === 'au' ) {
                if ( ( i > 3 ) && ( i < lu.length-1 ) &&
                     ( lu[i-1].phoneme === 'z' ) && ( lu[i-2].lettres === 'o' ) &&
                     ( lu[i+1].phoneme === 'r' ) )  { lu[i].phoneme = 'O'; i++; }  // -osAUr- => O ouvert
            }
        }
    }
};

/* Décompose un mot lu (tableau de phonèmes) en syllabes (tableau de tableau de phonèmes). */
dysaide_Engine.prototype.syllabes = function(lu) {
    lu = lu.slice(0).reverse();
    let res = [], syll = [];
    let v = false, c = false, h = [];
    let phoneme;
    let eng = this,
        nouv_syll = function() {
        if (h.length) {
            if (eng.est_voyelle(phoneme)) {
                res.push(syll.concat(h).reverse());
                syll = [phoneme];
            }
            else {
                res.push(syll.reverse());
                syll = h;
                syll.push(phoneme);
            }
            h = [];
        }
        else {
            res.push(syll.reverse());
            syll = [phoneme];
        }
        v = false; c = false;
    };
    let stock = function() {
        syll = syll.concat(h);
        syll.push(phoneme);
        h = [];
    };
    for (phoneme of lu) {
        if (this.est_voyelle(phoneme)) {
            if (phoneme.phoneme === 'h') { phoneme.phoneme = 'H'; }
            if (v && (v!=='i' || phoneme.lettres!=='u')) { nouv_syll(); }
            else   { stock(); }
            v = phoneme.lettres;
        }
        else
        if ( phoneme.phoneme === 'H' ) {
            h.push(phoneme);
        }
        else {
            if (v) {
                if (c) {
                    if ( (c === 'r') || (c === 'i') || ( (c === 'l') && (phoneme.lettres !== 'r') && (phoneme.lettres !== 's') ) ) {
                        c = true;
                        stock();
                    }
                    else {
                        nouv_syll();
                    }
                }
                else {
                    c = phoneme.lettres;
                    stock();
                }
            }
            else {
                stock();
            }
        }
    }
    if (v) {
        res.push(syll.concat(h).reverse());
        return res.reverse();
    }
    else {
        syll.reverse();
        if (res.length) {
            res.reverse();
            res[0] = syll.concat(res[0]);
            return res;
        }
        else {
            return [syll];
        }
    }
    /*
    voyelle ?
        >> déjà (v) et pas 'ui' ?
            >> nouvelle syllabe
            !> on a une voyelle (v = True) + Stock
        !> # ?
            >> Buffer
            !> v ?
                >> c ?
                    >> c==r | c==j | (c==l & this!=r) ?
                        >> c = 1
                        !> nouvelle syllabe
                    !> c = this + Stock
                !> Stock
    nouvelle syllabe:
        buffer ?
            >> voyelle ?
                >> stocker le contenu du buffer
                   enregistrer la précédente syllabe (reverse!)
                   en commencer une nouvelle avec la phonème en cours
                !> enregistrer la précédente syllabe (reverse!)
                   en commencer une nouvelle avec le buffer puis la phonème en cours
            !> enregistrer la précédente syllabe (reverse!)
               en commencer une nouvelle avec la phonème en cours
    stock:
        stock le buffer, stock la lettre
    */
};

/* Identifie les séparateurs de l'écriture inclusive et les remplace par la séparation choisie dans le texte 'txt'.
 * Les séparations sont fournie dans un tableau : corps.alt => corps(sep[0])alt(sep[1]) &  corps.alt.s => corps(sep[2])alt(sep[3])s
 * Si 'sep' n'est pas fourni, this.incl_sep est utilisé.
 */
dysaide_Engine.prototype.incl_formate = function(txt,sep) {
    if ( typeof(sep) === 'string' ) { sep = [sep,'',sep,sep]; }
    /*
    let repl =
        (this.options.incl.distrib[0]) ?
            function (match,prec,corps,p1,alt,s) {
                if (s) {
                    if (  alt !== 'e' ) { return prec+corps+'s'+sep[0]+alt+'s'+sep[1]; }
                                   else { return prec+corps+sep[2].replace('?',p1)+alt+sep[3].replace('?',s[0])+'s'; }
                }
                else {
                    return prec+corps+sep[0].replace('?',p1)+alt+sep[1];
                }
            }
        :   function (match,prec,corps,p1,alt,s) {
                return (s) ? (prec+corps+sep[2].replace('?',p1)+alt+sep[3].replace('?',s[0])+'s')
                           : (prec+corps+sep[0].replace('?',p1)+alt+sep[1]);
            };
    */
    let repl =
        function (match,prec,corps,p1,alt,p2,s) {
            if (!s) {
                let x = alt[alt.length-1];
                if (x === 's' || x === 'S') {
                    let y = corps[corps.length-1];
                    if (y !== 's' && y !== 'x' && y !== 'S' && y !== 'X') {
                        alt = alt.slice(0,-1);
                        s = x;
                        p2 = '#';
                    }
                }
            }
            if (s) {
                return (prec+corps+sep[2].replace('?',p1)+alt+sep[3].replace('?',p2)+s);
            }
            else {
                return (prec+corps+sep[0].replace('?',p1)+alt+sep[1]);
            }
        };
    return txt.replace(/(^|[^A-Za-zÀ-ýŒœ\/\.])([A-Za-zÀ-ýŒœ]+)([\.·•])(?!com|gov|edu|org|xyz|int|fr|be|ch|de|uk|ca|it|quebec)([A-Za-zÀ-ýŒœ]+)(?:([\.·•])([Ss])(?![A-Za-zÀ-ýŒœ]))?(?!\.?[A-Za-zÀ-ýŒœ\/])/g,repl);
};

/* Décompose le texte 'txt' (string) en série de phonèmes (dysaide_Phonème) ponctuations incluses. */
dysaide_Engine.prototype.lire_texte = function(txt) {
    // Mise en forme de l'écriture inclusive (si besoin)
    if (this.opt_incl) { txt = this.incl_formate(txt, ['#0?#','#1##','#2?#','#3?#'] ); }
    let lu = [], eng = this, buff = [];
    let push_ponct =
        (this.opt_ponc || this.opt_chif) ?
            function(ponct) {
                ponct.replace(/([^0-9]*)([0-9]*)/g, function(m,p,c) {
                    if (p) { lu.push(new dysaide_Phoneme('-',p)); }
                    if (c) { lu.push(new dysaide_Phoneme('7',c)); }
                    return '';
                });
            }
        :   function(ponct) {
                lu.push(new dysaide_Phoneme('-',ponct));
            };
    let avance =
        (this.syll_n>0) ?
            function (match,ponct,mot) {
                if (buff.length) {
                    if ( (ponct==="'") || (ponct==="’") ) {
                        buff.push( new dysaide_Phoneme('-',ponct) );
                        buff = buff.concat( eng.lire_mot(mot) );
                        return '';
                    }
                    lu = lu.concat( eng.syllabes(buff) );
                }
                if (ponct) {
                    push_ponct(ponct);
                }
                buff = eng.lire_mot(mot);
                return '';
            }
        :   function (match,ponct,mot) {
                if (ponct) {
                    push_ponct(ponct);
                }
                lu = lu.concat( eng.lire_mot(mot) );
                return '';
            };
    let incl_muet = function(match) {
        lu.push(new dysaide_Phoneme('H',match));
        return '';
    };
    while (txt) {
        if ( (txt[0]==='#') && /^#[0-3].#/.exec(txt.substr(0,4)) ) {
            if (buff.length) { lu = lu.concat( eng.syllabes(buff) ); buff = []; }
            lu.push( new dysaide_Phoneme('I'+txt[1],(txt[2]==='#')?'':txt[2]) );
            txt = txt.slice(4);
            txt = txt.replace(/^(es?|s)(?![A-Za-zÀ-ýŒœ])/i,incl_muet);
            continue;
        }
        txt = txt.replace(/^([^A-Za-zÀ-ýŒœ]*)([A-Za-zÀ-ýŒœ]*)/,avance);
    }
    if (buff.length) { lu = lu.concat( eng.syllabes(buff) ); }
    return lu;
};

/* Désaccentue les lettres accentuées qui n'ont pas à être distinguées de leur version sans accent. */
const dysaide_accents_map = {'à':'a','â':'a','ä':'a','ê':'è','ë':'è','ì':'i','î':'i','ï':'i','ò':'o','ô':'o','ö':'o','ñ':'n','ç':'C'};
dysaide_Engine.prototype.sans_accent = function(lettre) {
    lettre = lettre.toLowerCase();
    if (lettre in dysaide_accents_map) { return dysaide_accents_map[lettre]; }
    return lettre;
};

/* Renvoie le texte agrémenter des balises html <span> pour distinguer les lettres, phonèmes, syllabes, mots. */
dysaide_Engine.prototype.remplace_texte = function(txt) {
    if (this.opt_rien) { return txt; }
    if (this.syll_n>0 || this.opt_phon) {
        let lu = this.lire_texte(txt);
        let eng = this;
        txt = [];
        let phoneme;
        let do_ponct =
            (this.opt_mots) ?
                (this.opt_ponc) ?
                    function() {
                        if ( phoneme.lettres === ' ' ) {
                            txt += ' </span><span class="dysaide-mot">';
                        }
                        else {
                            txt += phoneme.lettres.replace(/([^\s-]*)(-*)(\s*)/g, function (x,c,t,s) {
                                return ( (c||t) ? ('<span class="dysaide-lett-_">'+c+t+'</span>') : '' ) +
                                       ( (t||s) ? (s+'</span><span class="dysaide-mot">') : '' );
                            });
                        }
                    }
                :   function() {
                        if ( phoneme.lettres === ' ' ) {
                            txt += ' </span><span class="dysaide-mot">';
                        }
                        else {
                            txt += phoneme.lettres.replace(/([^\s-]*)([\s-]*)/g, function (x,c,ts) {
                                return c + ( (ts) ? (ts+'</span><span class="dysaide-mot">') : '' );
                            });
                        }
                    }
            :   (this.opt_ponc) ?
                    function() { txt += phoneme.lettres.replace(/(\S+)/g, '<span class="dysaide-lett-_">$1</span>'); }
                :   function() { txt += phoneme.lettres; };
        let do_lett =
            (this.opt_lett) ?
                function() {
                    for (let lettre of phoneme.lettres) {
                        if (lettre in eng.opt_lett) {
                            txt += '<span class="dysaide-lett-'+eng.sans_accent(lettre)+'">'+lettre+'</span>';
                        }
                        else {
                            txt += lettre;
                        }
                    }
                }
            :   function() { txt += phoneme.lettres; };
        let do_phon =
            (this.opt_phon) ?
                (this.opt_incl) ?
                    function() {
                        if ( (phoneme.phoneme !== ' ') && (phoneme.phoneme !== '+') ) {
                            txt += '<span class="dysaide-phon-'+phoneme.phoneme+'">';
                            if (phoneme.phoneme[0]==='I') {
                                txt += '<span>'+phoneme.lettres+'</span>';
                            }
                            else {
                                do_lett();
                            }
                            txt += '</span>';
                        }
                        else {
                            do_lett();
                        }
                    }
                :   function() {
                        if ( (phoneme.phoneme !== ' ') && (phoneme.phoneme !== '+') ) {
                            txt += '<span class="dysaide-phon-'+phoneme.phoneme+'">';
                            do_lett();
                            txt += '</span>';
                        }
                        else {
                            do_lett();
                        }
                    }
            :   (this.opt_incl) ?
                    function() {
                        if ( typeof(phoneme.phoneme) === 'string' ) {
                            txt += '<span class="dysaide-phon-'+phoneme.phoneme+'"><span>'+phoneme.lettres+'</span></span>';
                        }
                        else {
                            do_lett();
                        }
                    }
                :   function() {
                        do_lett();
                    };
        let do_chiffre =
            (this.opt_chif) ?
                function() {
                    for (let lettre of phoneme.lettres) {
                        if (lettre in eng.opt_chif) {
                            txt += '<span class="dysaide-lett-'+eng.sans_accent(lettre)+'">'+lettre+'</span>';
                        }
                        else {
                            txt += lettre;
                        }
                    }
                }
            :   function() { txt += phoneme.lettres; };
        let do_elem = function() {
            switch (phoneme.phoneme) {
                case '-':
                    do_ponct();
                    break;
                case '7':
                    do_chiffre();
                    break;
                default:
                    do_phon();
            }
        };
        if (this.syll_n > 0) {
            let nouv = true;
            for (phoneme of lu) {
                if (phoneme instanceof dysaide_Phoneme) {
                    do_elem();
                    nouv = true;
                }
                else {
                    txt += '<span class="dysaide-syll-'+(this.syll_k++)+((nouv)?'':' dysaide-syll-sep')+'">';
                    if ( this.syll_k > this.syll_n ) { this.syll_k = 1; }
                    for (phoneme of phoneme) {
                        do_elem();
                    }
                    txt += '</span>';
                    nouv = false;
                }
            }
        }
        else {
            for (phoneme of lu) {
                do_elem();
            }
        }
        if (this.opt_mots) {
            let debmot = true;
            txt = txt.replace(/^(\s*)<\/span>/, (m,s) => { debmot = false; return s; });
            if (debmot) { txt = '<span class="dysaide-mot">'+ txt; }
            if (txt.slice(-26) === '<span class="dysaide-mot">') {
                txt = txt.slice(0,-26);
            }
            else {
                txt += '</span>';
            }
        }
        return txt;
    }
    else {
        if (this.opt_incl || this.lett || this.ponc) {
            //  txt.replace(/(?:#([0123])(.?)#|([éu])|([^\s0-9A-Za-zÀ-ýŒœ]))/g,
            //      function(m,i1,i2,p,l) {
            //          return (i1) ? ('<span class="dysaide-phon-I'+i1+'"><span>'+i2+'</span></span>') :
            //                 (l)  ? ('<span class="dysaide-lett-'+eng.sans_accent(l)+'">'+l+'</span>') :
            //                        ('<span class="dysaide-lett-_">'+p+'</span>');
            //      }
            //  )
            let eng = this,
                repl,
                regexp = [],
                repl_arg = [],
                repl_res = '';
            if (this.opt_incl) {
                txt = this.incl_formate(txt, ['#0?#','#1##','#2?#','#3?#'] );
                regexp = ['(?:#)([0123])(.)#'];
                repl_arg = ['i1','i2'];
                repl_res = (this.opt_lett || this.opt_ponc) ? "(i1)?('<span class=\"dysaide-phon-I'+i1+'\"><span>'+(i2==='#'?'':i2)+'</span></span>'):"
                                                            :      "('<span class=\"dysaide-phon-I'+i1+'\"><span>'+(i2==='#'?'':i2)+'</span></span>')";
            }
            if (this.opt_lett) {
                let r = '';
                for (let l in this.opt_lett) {
                    r += l;
                }
                for (let l in dysaide_accents_map) {
                    if (dysaide_accents_map[l] in this.opt_lett) {
                        r += l;
                    }
                }
                regexp.push('(['+r+'])');
                repl_arg.push('l');
                repl_res += (this.opt_ponc) ? "(l)?('<span class=\"dysaide-lett-'+eng.sans_accent(l)+'\">'+l+'</span>'):"
                                            :     "('<span class=\"dysaide-lett-'+eng.sans_accent(l)+'\">'+l+'</span>')";
            }
            if (this.opt_ponc) {
                regexp.push('([^\\s0-9A-Za-zÀ-ýŒœ])');
                repl_arg.push('p');
                repl_res += "('<span class=\"dysaide-lett-_\">'+p+'</span>')";
            }
            regexp = new RegExp(regexp.join('|'),'gi');
            eval( "repl=function(m,"+repl_arg.join(',')+"){return "+repl_res+";};" );
            if (this.opt_mots) {
                return txt.replace(/([^\s-]+[\s-]*)/g, function(m) { return '<span class="dysaide-mot">'+m.replace(regexp,repl)+'</span>'; });
            }
            else {
                return txt.replace(regexp,repl);
            }
        }
        else {
            return txt.replace(/([^\s-]+[\s-]*)/g,'<span class="dysaide-mot">$1</span>');
        }
    }
};

/* Réalise le traitement remplace_texte de manière récursive dans un élément html. */
dysaide_Engine.prototype.remplacement_recursif = function(el) {
    if (el.nodeType === 1 && el.className.substr(0,8) === 'dysaide-') { return; }
    el.classList.add('dysaide-gene');
    let liste = [];
    for (el of el.childNodes) {
        liste.push(el);
    }
    for (el of liste) {
        if (el.nodeType === 3) {
            let newnode = document.createElement('span');
            newnode.className = 'dysaide-gene';
            newnode.innerHTML = DOMPurify.sanitize(this.remplace_texte(el.textContent));
            el.parentNode.replaceChild(newnode,el);
        }
        else
        if (el.nodeType === 1 && el.nodeName !== 'SCRIPT') {
            this.remplacement_recursif(el);
            if (el.nodeName === 'A') { el.classList.add('dysaide-liens'); }
        }
    }
};

/* Colore les lignes des éléments d'un élément HTML et de ses sous-éléments (en supposant les mots déjà détectés). */
dysaide_Engine.prototype.colore_lignes = function(el) {
    let mots = (typeof el === 'undefined') ? document.getElementsByClassName('dysaide-mot')
                                           : el.getElementsByClassName('dysaide-mot');
    for(let i=0, n=mots.length; i<n; i++) {
        let m = mots[i];
        let newOffset = m.offsetTop;
        if (newOffset !== this.ligne_offset) {
            this.ligne_k = 3 - this.ligne_k;
            this.ligne_offset = newOffset;
        }
        m.className = 'dysaide-mot dysaide-ligne-'+this.ligne_k;
    }
};

/* Crée et renvoie le css correspondant aux options. */
dysaide_Engine.prototype.css = function() {
    let css = '';
    // Règles générales
    let rule = this.options.gene;
    if ( rule.actif ) {
        let css_rules = [];
        if ( rule.police[0]      ) { css_rules.push( 'font-family:"'   +         rule.police[1]         + '"  !important' ); }
        if ( rule.esp_lettres[0] ) { css_rules.push( 'letter-spacing:' +         rule.esp_lettres[1]    + 'em !important' ); }
        if ( rule.esp_mots[0]    ) { css_rules.push( 'word-spacing:'   +         rule.esp_mots[1]       + 'em !important' ); }
        if ( rule.esp_lignes[0]  ) { css_rules.push( 'line-height:'    + (Number(rule.esp_lignes[1])+1) +   ' !important' ); }
        if ( rule.taille[0]      ) { css_rules.push( 'font-size:'      +         rule.taille[1]         + 'px !important' ); }
        if ( css_rules.length>0 ) { css += '.dysaide-gene{'+css_rules.join(';')+'}'; }
        if ( rule.diff_lignes[0] ) {
            let r = rule.diff_lignes[1];
            if ( r.col[0] ) { for (let i of [1,2]) { css += '.dysaide-ligne-'+i+'{color:'           +r.col[i]+' !important}'; } } 
            if ( r.bg[0]  ) { for (let i of [1,2]) { css += '.dysaide-ligne-'+i+'{background-color:'+r.bg[i] +' !important}'; } }
        }
        if ( rule.liens[0] ) {
            let r = rule.liens[1];
            if ( r.col[0] ) { css += 'a.dysaide-liens,a.dysaide-liens .dysaide-ligne-1,a.dysaide-liens .dysaide-ligne-2{color:'           +r.col[1]+' !important}'; } 
            if ( r.bg[0]  ) { css += 'a.dysaide-liens,a.dysaide-liens .dysaide-ligne-1,a.dysaide-liens .dysaide-ligne-2{background-color:'+r.bg[1] +' !important}'; }
        }
    }
    // Syllabes
    rule = this.options.syll;
    if ( rule.actif && rule.diff[0] ) {
        for (let i = 1, n = rule.diff[1].nb; i <= n; i++) {
            let sel = '.dysaide-syll-'+i;
            if (this.testmode) { for (let k = i+n; k < 7; k += n) { sel += ',.dysaide-syll-'+k; } }
            let css_rules = [];
            if ( rule.diff[1].col[0] ) { css_rules.push( 'color:'            + rule.diff[1].col[i] + ' !important' ); }
            if ( rule.diff[1].bg[0]  ) { css_rules.push( 'background-color:' + rule.diff[1].bg[i]  + ' !important' ); }
            if ( css_rules.length>0 ) { css += sel+'{'+css_rules.join(';')+'}'; }
        }
    }
    if ( rule.actif && rule.sep[0] ) {
        css += '.dysaide-syll-sep::before{content:"'+this.syll_sep+'"}';
    }
    // Phonèmes & lettres
    for (let opt of ['phon','lett']) {
        rule = this.options[opt];
        if ( rule.actif ) {
            for (let phoneme in rule) {
                if ( phoneme.length<2 && rule[phoneme][0] ) {
                    let css_rules = [];
                    if ( rule[phoneme][1].col[0] ) { css_rules.push( 'color:'            + rule[phoneme][1].col[1] + ' !important' ); }
                    if ( rule[phoneme][1].bg[0]  ) { css_rules.push( 'background-color:' + rule[phoneme][1].bg[1]  + ' !important' ); }
                    if ( rule[phoneme][1].s[0]   ) { css_rules.push( 'text-decoration:'  + 'underline'             + ' !important' ); }
                    if ( rule[phoneme][1].i[0]   ) { css_rules.push( 'font-style:'       + 'italic'                + ' !important' ); }
                    if ( rule[phoneme][1].g[0]   ) { css_rules.push( 'font-weight:'      + 'bold'                  + ' !important' ); }
                    if ( css_rules.length>0 ) { css += '.dysaide-'+opt+'-'+phoneme+'{'+css_rules.join(';')+'}'; }
                }
            }
        }
    }
    // Points inclusifs
    rule = this.options.incl;
    if ( rule.actif ) {
        if (rule.form[0]) {
            let css_rules = [];
            if ( rule.form[1].col[0] ) { css_rules.push( 'color:'            + rule.form[1].col[1] + ' !important' ); }
            if ( rule.form[1].bg[0]  ) { css_rules.push( 'background-color:' + rule.form[1].bg[1]  + ' !important' ); }
            if ( css_rules.length>0 ) {
                let css_sel = []; for (let i of [0,1,2,3]) { css_sel.push('.dysaide-phon-I'+i); }
                css += css_sel.join(',')+'{'+css_rules.join(';')+'}';
            }
        }
        if ( rule.sep[0] ) {
            let css_sel = []; for (let i of [0,1,2,3]) { css_sel.push('.dysaide-phon-I'+i+' span'); }
            css += css_sel.join(',') + '{display:none}';
            css += '.dysaide-phon-I0::after{content:"'+this.incl_sep[0]+'"}';
            css += '.dysaide-phon-I1::after{content:"'+this.incl_sep[1]+'"}';
            css += '.dysaide-phon-I2::after{content:"'+this.incl_sep[2]+'"}';
            css += '.dysaide-phon-I3::after{content:"'+this.incl_sep[3]+'"}';
        }
    }
    return css;
};

