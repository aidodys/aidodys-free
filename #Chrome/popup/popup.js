/**
 * This module is part of DysAide.
 * 
 * @author   Bunker D
 * @contact  contact@bunkerd.fr
 * @version  0.1
 * @since    0.1
 * @license  GNU GPLv3
 *
 * GNU General Public Licence (GPL) version 3
 *
 * DysAide is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * DysAide is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with
 * DysAide; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA4z
 */


// Set the option states
chrome.storage.sync.get(['options','on','ctrl'], function(options) {
    let ctrl = options.ctrl || {ctrl:true,maj:false,f8:true};
    document.getElementById('cmd').textContent =
        (ctrl.ctrl) ? ( ((/mac/i.test(navigator.platform))?'Cmd':'Ctrl') + ((ctrl.maj)?'+Maj':'') + ' (maintenu)' + ((ctrl.f8)?' ou F8':'') ) : 'F8';
    let on = options.on || (typeof(options.on) === 'undefined');
    options = options.options || {
            'gene' : {
                'actif'       :  true,
                'police'      : [true , 'Lexie Readable' ],
                'taille'      : [false, 18  ],
                'esp_lettres' : [true , 0.2 ],
                'esp_mots'    : [true , 0.7 ],
                'esp_lignes'  : [true , 1   ],
                'diff_lignes' : [false, { 'col' : [false, '#0fa4f6', '#f19500' ] ,
                                          'bg'  : [true , '#ccfeff', '#fff879' ] }],
                'liens'       : [false, { 'col' : [false, '#008d31' ] ,
                                          'bg'  : [true , '#aef4bb' ] }]
            },
            'syll' : {
                'actif' :  false,
                'oral'  : [false],
                'diff'  : [true , { 'nb'  : 3 ,
                                    'col' : [false, '#555af8', '#fc3533', '#16b716' ] ,
                                    'bg'  : [true , '#ccfeff', '#ffb6a6', '#fff879' ] }],
                'sep'   : [false, '-' ]
            },
            'phon' : {
                'actif' :  false,
                'q_fin' : [false],
                'H'     : [true , { 'col' : [true, '#a0a0a0' ], 'bg' : [false, '#dbdbdb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'a'     : [false, { 'col' : [true, '#b56912' ], 'bg' : [false, '#fff9b1' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                '2'     : [true , { 'col' : [true, '#6a0000' ], 'bg' : [false, '#dbbaba' ], 's' : [true ], 'i' : [false], 'g' : [true ] }],
                '9'     : [true , { 'col' : [true, '#6a0000' ], 'bg' : [false, '#dbbaba' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'q'     : [false, { 'col' : [true, '#6a0000' ], 'bg' : [false, '#dbbaba' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'e'     : [true , { 'col' : [true, '#d81422' ], 'bg' : [false, '#ff8db8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'E'     : [true , { 'col' : [true, '#f82eff' ], 'bg' : [false, '#ed97ff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'i'     : [false, { 'col' : [true, '#0fdd00' ], 'bg' : [false, '#bfffc9' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'j'     : [true , { 'col' : [true, '#006c27' ], 'bg' : [false, '#99c697' ], 's' : [false], 'i' : [true ], 'g' : [false] }],
                'o'     : [true , { 'col' : [true, '#1500cf' ], 'bg' : [false, '#d3e8ff' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'O'     : [true , { 'col' : [true, '#1500cf' ], 'bg' : [false, '#d3e8ff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'y'     : [false, { 'col' : [true, '#14ab79' ], 'bg' : [false, '#6aefce' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'u'     : [true , { 'col' : [true, '#0fb0d9' ], 'bg' : [false, '#a7f4fb' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '3'     : [true , { 'col' : [true, '#ecae08' ], 'bg' : [false, '#ffe998' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'A'     : [true , { 'col' : [true, '#4e41ff' ], 'bg' : [false, '#c4d1ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '0'     : [true , { 'col' : [true, '#ff660f' ], 'bg' : [false, '#ffb19f' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '1'     : [true , { 'col' : [true, '#7724bf' ], 'bg' : [false, '#dcb7ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                '5'     : [true , { 'col' : [true, '#7724bf' ], 'bg' : [false, '#dcb7ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'b'     : [false, { 'col' : [true, '#bb63d0' ], 'bg' : [false, '#f0ccff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'k'     : [false, { 'col' : [true, '#38f075' ], 'bg' : [false, '#b4ffb0' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'S'     : [false, { 'col' : [true, '#75bc22' ], 'bg' : [false, '#cde8a6' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'd'     : [false, { 'col' : [true, '#e5750a' ], 'bg' : [false, '#f2c395' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'f'     : [false, { 'col' : [true, '#da5db9' ], 'bg' : [false, '#ecb5dd' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'g'     : [false, { 'col' : [true, '#8fc569' ], 'bg' : [false, '#c8e1b7' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'Z'     : [false, { 'col' : [true, '#fd2966' ], 'bg' : [false, '#fd2966' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'N'     : [false, { 'col' : [true, '#e47646' ], 'bg' : [false, '#f3bb8c' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'l'     : [false, { 'col' : [true, '#4f10df' ], 'bg' : [false, '#ad92e9' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'm'     : [false, { 'col' : [true, '#dcbe0a' ], 'bg' : [false, '#f0e394' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'n'     : [false, { 'col' : [true, '#bf8f72' ], 'bg' : [false, '#dec7ba' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'G'     : [false, { 'col' : [true, '#2067e2' ], 'bg' : [false, '#a0bef4' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'p'     : [false, { 'col' : [true, '#3295c3' ], 'bg' : [false, '#9cc9df' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'r'     : [false, { 'col' : [true, '#c88cf4' ], 'bg' : [false, '#e5cdf6' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                's'     : [false, { 'col' : [true, '#19bfad' ], 'bg' : [false, '#a2f8ee' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'z'     : [false, { 'col' : [true, '#0c7ebf' ], 'bg' : [false, '#42b2f8' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                't'     : [false, { 'col' : [true, '#a33238' ], 'bg' : [false, '#d19699' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'v'     : [false, { 'col' : [true, '#dabf16' ], 'bg' : [false, '#f9f6cd' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'w'     : [false, { 'col' : [true, '#4a3bb8' ], 'bg' : [false, '#a7a1dc' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'x'     : [false, { 'col' : [true, '#bf408b' ], 'bg' : [false, '#dea3c6' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'X'     : [false, { 'col' : [true, '#bf408b' ], 'bg' : [false, '#dea3c6' ], 's' : [false], 'i' : [false], 'g' : [true ] }]
            },
            'lett' : {
                'actif' :  false,
                '_'     : [false, { 'col' : [true, '#7b1411' ], 'bg' : [false, '#b25d5d' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'a'     : [false, { 'col' : [true, '#72f472' ], 'bg' : [false, '#c0fac3' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'b'     : [true , { 'col' : [true, '#cb1616' ], 'bg' : [false, '#ffec5e' ], 's' : [false], 'i' : [true ], 'g' : [false] }],
                'c'     : [false, { 'col' : [true, '#ff9900' ], 'bg' : [false, '#ffea72' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'C'     : [false, { 'col' : [true, '#c77700' ], 'bg' : [false, '#eeb988' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'd'     : [true , { 'col' : [true, '#261bcb' ], 'bg' : [false, '#a0d1ff' ], 's' : [false], 'i' : [true ], 'g' : [false] }],
                'e'     : [false, { 'col' : [true, '#0b93d6' ], 'bg' : [false, '#92cfeb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'é'     : [false, { 'col' : [true, '#4c1798' ], 'bg' : [false, '#a186cb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'è'     : [false, { 'col' : [true, '#d740ee' ], 'bg' : [false, '#f2b1fb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'f'     : [false, { 'col' : [true, '#70dc47' ], 'bg' : [false, '#bdebac' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'g'     : [false, { 'col' : [true, '#bc9c4e' ], 'bg' : [false, '#dbcda8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'h'     : [false, { 'col' : [true, '#0daf6a' ], 'bg' : [false, '#8bddba' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'i'     : [false, { 'col' : [true, '#681eff' ], 'bg' : [false, '#ae93ff' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'j'     : [false, { 'col' : [true, '#c11d0a' ], 'bg' : [false, '#e0938a' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'k'     : [false, { 'col' : [true, '#cf8b1a' ], 'bg' : [false, '#edcf99' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'l'     : [false, { 'col' : [true, '#00d2bd' ], 'bg' : [false, '#5edce8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'm'     : [false, { 'col' : [true, '#3ca108' ], 'bg' : [false, '#3dff07' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'n'     : [true , { 'col' : [true, '#d1881d' ], 'bg' : [false, '#ffec5e' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'o'     : [false, { 'col' : [true, '#3a8fe3' ], 'bg' : [false, '#aed2f8' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'p'     : [true , { 'col' : [true, '#2a8906' ], 'bg' : [false, '#7dff76' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'q'     : [true , { 'col' : [true, '#c90bd1' ], 'bg' : [false, '#faa2ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'r'     : [false, { 'col' : [true, '#72a4bf' ], 'bg' : [false, '#bad1de' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                's'     : [false, { 'col' : [true, '#1cb2fc' ], 'bg' : [false, '#a8e6fe' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                't'     : [false, { 'col' : [true, '#0f1e98' ], 'bg' : [false, '#838acb' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'u'     : [true , { 'col' : [true, '#46b7af' ], 'bg' : [false, '#6de9ff' ], 's' : [true ], 'i' : [false], 'g' : [false] }],
                'v'     : [false, { 'col' : [true, '#a83bd1' ], 'bg' : [false, '#ff89f1' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                'w'     : [false, { 'col' : [true, '#9a4bb6' ], 'bg' : [false, '#d0a9dc' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'x'     : [false, { 'col' : [true, '#8bc227' ], 'bg' : [false, '#cbe69d' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'y'     : [false, { 'col' : [true, '#25c46f' ], 'bg' : [false, '#9be5be' ], 's' : [false], 'i' : [false], 'g' : [false] }],
                'z'     : [false, { 'col' : [true, '#1e00b4' ], 'bg' : [false, '#09b7fc' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '0'     : [true , { 'col' : [true, '#9ac900' ], 'bg' : [false, '#b9d856' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '1'     : [true , { 'col' : [true, '#008d31' ], 'bg' : [false, '#61f494' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '2'     : [true , { 'col' : [true, '#ce009e' ], 'bg' : [false, '#ef71d1' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '3'     : [true , { 'col' : [true, '#00c4d4' ], 'bg' : [false, '#77dce5' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '4'     : [true , { 'col' : [true, '#adc71d' ], 'bg' : [false, '#d1e369' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '5'     : [true , { 'col' : [true, '#e08b00' ], 'bg' : [false, '#e0b671' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '6'     : [true , { 'col' : [true, '#dd0000' ], 'bg' : [false, '#f58a8a' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '7'     : [true , { 'col' : [true, '#a409c9' ], 'bg' : [false, '#e57ffd' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '8'     : [true , { 'col' : [true, '#d82184' ], 'bg' : [false, '#ea7fb9' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
                '9'     : [true , { 'col' : [true, '#2100c4' ], 'bg' : [false, '#a493ff' ], 's' : [false], 'i' : [false], 'g' : [true ] }],
            },
            'incl' : {
                'actif'   :  true ,
                'form'    : [false, { 'col' : [true , '#9c10d7' ], 'bg' : [false, '#95ae9c' ] }],
                'sep'     : [true , '·' ],
                'distrib' : [false]
            }
        };
    let style_el = document.createElement('style');
    style_el.textContent = 'body{font-family:'+options.gene.police[1]+'}';
    document.body.appendChild(style_el);
    let x = document.getElementById('sw-on-off');
    x.checked = on;
    let onclass = document.getElementById( 'on-text').classList,
       offclass = document.getElementById('off-text').classList;
    if (on) {  onclass.add('actif'); }
    else    { offclass.add('actif'); document.getElementById('options').className = 'off'; }
    x.onclick = function() {
        on = !on;
        document.getElementById('options').className = (on) ? '' : 'off';
        onclass.toggle('actif');
        offclass.toggle('actif');
        chrome.storage.sync.set({'on':on});
    };
    for (let opt of ['gene','syll','phon','lett','incl']) {
        x = document.getElementById('sw-'+opt);
        x.checked = options[opt].actif;
        x.onclick = function() {
            options[opt].actif = this.checked;
            chrome.storage.sync.set({'options':options});
        };
    }
    document.getElementById('configlink').onclick =
        () => chrome.tabs.create({'url': "/options/options.html"});
});
