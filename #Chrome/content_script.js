/**
 * This module is part of DysAide.
 * 
 * @author   Bunker D
 * @contact  contact@bunkerd.fr
 * @version  0.1
 * @since    0.1
 * @license  GNU GPLv3
 *
 * GNU General Public Licence (GPL) version 3
 *
 * DysAide is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * DysAide is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with
 * DysAide; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA4z
 */


let style_el,
    on,
    ctrl,
    options = 'unset',
    dysaide_engine,
    highlighted_el,
    highlighted_el_classes,
    resize_timeout,
    actif = false,
    est_actif;

style_el = document.createElement('style');
style_el.textContent = '.dysaidehighlight{background-color:#ffce71 !important;color:#f51616 !important}';
document.body.appendChild(style_el);

style_el = document.createElement('style');
document.body.appendChild(style_el);


let fontcss = '@font-face {font-family: "Open Dyslexic";font-weight: bold;font-style: italic, oblique;src: url("$$$/OpenDyslexic-BoldItalic.otf");}@font-face {font-family: "Open Dyslexic";font-style: italic, oblique;src: url("$$$/OpenDyslexic-Italic.otf");}@font-face {font-family: "Open Dyslexic";font-weight: bold;src: url("$$$/OpenDyslexic-Bold.otf");}@font-face {font-family: "Open Dyslexic";src: url("$$$/OpenDyslexic-Regular.otf");}@font-face {font-family: "Andika New Basic";font-weight: bold;font-style: italic, oblique;src: url("$$$/AndikaNewBasic-BI.ttf");}@font-face {font-family: "Andika New Basic";font-weight: bold;src: url("$$$/AndikaNewBasic-B.ttf");}@font-face {font-family: "Andika New Basic";font-style: italic, oblique;src: url("$$$/AndikaNewBasic-I.ttf");}@font-face {font-family: "Andika New Basic";src: url("$$$/AndikaNewBasic-R.ttf");}@font-face {font-family: "Lexie Readable";font-weight: bold;src: url("$$$/LexieReadable-Bold.ttf");}@font-face {font-family: "Lexie Readable";src: url("$$$/LexieReadable-Regular.ttf");}';
fontcss = fontcss.replace(/\$\$\$/g,chrome.runtime.getURL('fonts'))

function tracking(e) {
    if (!est_actif(e)) { untrack(); return; }
    e = e.target;
    if (e === highlighted_el) { return; }
    if (highlighted_el) { highlighted_el_classes.remove('dysaidehighlight'); }
    highlighted_el = e;
    highlighted_el_classes = highlighted_el.classList;
    highlighted_el_classes.add('dysaidehighlight');
}
function tracking_(e) {
    highlighted_el = e.target;
}
function track() {
    if (ctrl.track) {
        document.removeEventListener('mousemove', tracking_);
        if (highlighted_el) {
            highlighted_el_classes = highlighted_el.classList;
            highlighted_el_classes.add('dysaidehighlight');
        }
    }
    document.addEventListener('mousemove', tracking);
    document.removeEventListener('keydown', listendown);
    document.addEventListener('keyup', listenup);
    document.addEventListener('click', replaceonclick);
}
function untrack() {
    document.removeEventListener('mousemove', tracking);
    if (highlighted_el) { highlighted_el_classes.remove('dysaidehighlight'); }
    if (ctrl.track) {
        document.addEventListener('mousemove', tracking_);
    }
    else {
        highlighted_el = false;
    }
    document.addEventListener('keydown', listendown);
    document.removeEventListener('keyup', listenup);
    document.removeEventListener('click', replaceonclick);
}
function listenup(e) {
    if (!est_actif(e)) { untrack(); }
}
function listendown(e) {
    if (est_actif(e)) { track(); }
}
function listenF8(e) {
    if (e.keyCode === 119) {
        if (actif) {
            actif = false;
            untrack();
        }
        else {
            actif = true;
            track();
        }
    }
}
function replaceonclick(e) {
    if (!est_actif(e)) { untrack(); return; }
    if (!highlighted_el) { tracking(e); return; }
    dysaide_engine.remplacement_recursif(highlighted_el);
    if (dysaide_engine.options.gene.diff_lignes[0]) {
        window.setTimeout(() => dysaide_engine.colore_lignes(highlighted_el), 100);
    }
}

function set() {
    let set_on = function() {
        dysaide_engine = new dysaide_Engine(options);
        style_el.innerHTML = DOMPurify.sanitize(fontcss + dysaide_engine.css());
        document.addEventListener('keydown', listendown);
        if (dysaide_engine.options.gene.diff_lignes[0]) {
            window.addEventListener("resize", function() {
                window.clearTimeout(resize_timeout);
                resize_timeout = window.setTimeout(() => dysaide_engine.colore_lignes(), 100);
            });
        }
        if (ctrl.f8) {
            est_actif =
                (ctrl.ctrl) ?
                    (ctrl.maj) ?
                        function (e) { return actif || ( (e.ctrlKey || e.metaKey) && e.shiftKey ); }
                    :   function (e) { return actif || e.ctrlKey || e.metaKey; }
                :   function (e) { return actif; };
            document.addEventListener('keydown', listenF8);            
        }
        else {
            est_actif =
                (ctrl.maj) ?
                    function (e) { return (e.ctrlKey || e.metaKey) && e.shiftKey; }
                :   function (e) { return e.ctrlKey || e.metaKey; };
        }
        if (ctrl.track) {
            document.addEventListener('mousemove', tracking_);
        }
    };
    if (on) {
        if (options === 'unset') {
            chrome.storage.sync.get(['options','ctrl'], function(s) {
                options = s.options;
                ctrl = s.ctrl || {ctrl:true,maj:false,f8:true,track:true};
                set_on();
            });
        }
        else {
            set_on();
        }
    }
    else {
        untrack();
        style_el.textContent = '';
        document.removeEventListener('keydown', listendown);
        document.removeEventListener('keydown', listenF8);
        document.removeEventListener('keyup', listenup);
        document.removeEventListener('click', replaceonclick);
        style_el. textContent = '';
    }
}

chrome.storage.sync.get(['on'], function(s) {
    on = (typeof s.on === 'undefined') ? true : s.on;
    set();
});

chrome.storage.onChanged.addListener(function(changes) {
    if ('on' in changes) { on = changes.on.newValue; }
    if ('options' in changes) { options = changes.options.newValue; }
    if ('ctrl' in changes) { ctrl = changes.options.newValue; }
    set();
});


