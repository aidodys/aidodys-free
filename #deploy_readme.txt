L'implémentation des extensions Chrome et WebExtension (Firefox) nécessitent certaines commandes spécifiques à l'un ou l'autre des environnements.
Néanmoins, préserver deux dossiers disjoints et devoir dupliquer chaque modification est pesant et source d'erreurs.
Le script #deploy.py permet de ne travailler qu'avec un seul ensembles de fichiers, puis de produire les dossiers spécifiques nécessaires à Chrome et Firefox.

Ce script fonctionne ainsi :
	Pour chaque navigateur (Chrome et Firefox), un dossier #<navigateur> (i.e. #Chrome ou #Firefox) est créé ou réinitialisé.
	Tous les dossiers et fichiers du dossier de départ y sont copiés, exceptés ceux dont le nom commence par un '.' (i.e. .gitignore) ou un '#' (i.e. #deploy.py).
	Lors de cette copie, les fichiers .js, .json et .css font l'objet d'une réécriture, afin de préserver uniquement les lignes correspondant au navigateur.
	L'encodage dans ces fichiers ce fait ainsi :
		Une ligne '//! <navigateur>' indique que la ligne suivante concernera uniquement le navigateur en question.
		Une ligne '//!! <navigateur>' indique que toutes les lignes suivantes, jusqu'à la prochaine ligne commençant par '//!', concerneront uniquement le navigateur en question.
		Parmi ces lignes à sélectionner, lorsqu'une ligne commence par '//?', ce préfixe sera retirée. (Cela permet de commenter la ligne dans le dossier de développement.)
	
	Par exemple, le navigateur princiupalement utilisé pour le développement est pour l'instant Chrome. Les fichiers sur lesquels on travaille doivent être compatibles Chrome.
	On commentera donc les lignes alternatives pour Firefox, et écrira par exemple :
			if ($('#default').hasClass('disabled')) {
				//!! Chrome
				chrome.storage.sync.remove('options');
				chrome.storage.sync.remove('ctrl');
				//!! Firefox
				//?browser.storage.sync.remove('options');
				//?browser.storage.sync.remove('ctrl');
				//!
			}
	Lors de la copie du fichier dans #Chrome, le script gardera :
			if ($('#default').hasClass('disabled')) {
				chrome.storage.sync.remove('options');
				chrome.storage.sync.remove('ctrl');
			}
	Tandis que lors de la copie du ficher dans #Firefox, le script gardera :
			if ($('#default').hasClass('disabled')) {
				browser.storage.sync.remove('options');
				browser.storage.sync.remove('ctrl');
			}

Le script, développé et testé sous Python 2.7, doit être lancé depuis la racine du projet (où le script #deploy.py se trouve).

N'oubliez pas que toute modification faite dans les dossiers d'exportation #Chrome et #Firefox seront supprimées !
Des modifications peuvent y être faites pour par exemple tester les corrections à faire pour Firefox, mais ces modifications doivent être portées dans les fichiers de développement !
