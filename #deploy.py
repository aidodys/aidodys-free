import os
import shutil


def rewritefile(infile, outfile, selector):
    """
    Rewrite a file, without keeping non-selected lines.

    In the input file, lines starting with //! indicate selectors:
        +--------------------+-------------------------------------------------------------------------------+
        | Line               | Effect                                                                        |
        +====================+===============================================================================+
        | //!\ *<selector>*  | The following line is kept only if the selector matches the input selector.   |
        +--------------------+-------------------------------------------------------------------------------+
        | //!!\ *<selector>* | The following lines are kept only if the selector matches the input selector. |
        |                    | Selection stops at the next line starting with //!.                           |
        +--------------------+-------------------------------------------------------------------------------+
        | //?\ *<code>*      | If the line does not have to be ignored, the starting //? will by removed.    |
        +--------------------+-------------------------------------------------------------------------------+

    :param str infile:
        Input file (path) to rewrite
    :param str outfile:
        Output file (path)
    :param str selector:
        Selector
    """
    selector = selector.lower()
    fi = open(infile, 'r')
    fo = open(outfile, 'w')
    do = [True, True]
    for line in fi:
        stripped = line.lstrip()
        if stripped[:3] == '//!':
            do[1] = True
            line = stripped[3:].lstrip('!').strip()
            if line and line.lower() != selector:
                do[stripped[3] == '!'] = False
        else:
            if do[0] and do[1]:
                if stripped[:3] == '//?':
                    fo.write(line[:(len(line)-len(stripped))]+stripped[3:])
                else:
                    fo.write(line)
            do[0] = True
    fi.close()
    fo.close()


def rewritedir(directory, selector):
    """
    Inside the input directory, create a directory named #*<selector>* with rewritten
    filled with all the files of the directory, rewritten by applying the selector.
    Files and folders starting with '#' and '.' are ignored.

    :param str directory: Directory to copy with line selection
    :param selector: Selector to apply
    """
    todir = os.path.join(directory, '#' + selector)
    if os.path.exists(todir):
        shutil.rmtree(todir)
    os.mkdir(todir)

    def rewrite(d0, d1):
        for f in os.listdir(d0):
            if f[0] in '.#':
                continue
            f0 = os.path.join(d0, f)
            f1 = os.path.join(d1, f)
            if os.path.isdir(f0):
                os.mkdir(f1)
                rewrite(f0, f1)
            else:
                if os.path.splitext(f)[1] in ['.js', '.json', '.css']:
                    rewritefile(f0, f1, selector)
                else:
                    shutil.copy(f0, f1)

    rewrite(directory, todir)


for sel in ['Chrome', 'Firefox']:
    rewritedir('.', sel)

