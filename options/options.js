/**
 * This module is part of DysAide.
 * 
 * @author   Bunker D
 * @contact  contact@bunkerd.fr
 * @version  0.1
 * @since    0.1
 * @license  GNU GPLv3
 *
 * GNU General Public Licence (GPL) version 3
 *
 * DysAide is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * DysAide is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * You should have received a copy of the GNU General Public License along with
 * DysAide; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA4z
 */

//! Chrome
chrome.storage.sync.get(['options','ctrl'], function(options) {
//! Firefox
//?browser.storage.sync.get(['options','ctrl']).then( function(options) {
    
    let ctrl0 = options.ctrl,
        ctrl = ctrl0 || {ctrl:true,maj:false,f8:true,track:true};
    options = options.options;
    
    let dysaide_engine;
    if (options) {
        dysaide_engine = new dysaide_Engine($.extend(true,{},options),true);
        $('#default').removeClass('disabled');
    }
    else {
        dysaide_engine = new dysaide_Engine(false,true);
        if (ctrl0) {
            $('#default').removeClass('disabled');
        }
    }
    
    /***************************
    *   COMPLÉTION DE L'HTML   *
    ***************************/

    $('.match').each( function() {
        let t = $(this).text().split('/');
        let res =  '<label class="switch small">'+
                        '<input type="checkbox" id="sw-'+t[0]+'">'+
                        '<span class="slider"></span>'+
                    '</label>'+
                    '<div class="match-in">'+t[1]+'</div>'+
                    ( (t.length>2) ? ('<div class="match-ex">'+t[2]) : '<div class="match-ex filler">');
        for (let i = 3; i < t.length;) {
            res += '<span class="ex-'+t[0]+'">'+t[i++]+'</span>'+t[i++];
        }
        res += '</div>'+
                '<label class="subcheckbox">'+
                    '<input type="checkbox" id="sw-'+t[0]+'-col">'+
                    '<div class="subcheckbox-box"></div>'+
                '</label>'+
                '<input class="jscolor" id="'+t[0]+'-col-1">'+
                '<label class="subcheckbox">'+
                    '<input type="checkbox" id="sw-'+t[0]+'-bg">'+
                    '<div class="subcheckbox-box"></div>'+
                '</label>'+
                '<input class="jscolor" id="'+t[0]+'-bg-1">'+
                '<label class="subcheckbox">'+
                    '<input type="checkbox" id="sw-'+t[0]+'-s">'+
                    '<div class="subcheckbox-box"></div>'+
                '</label>'+
                '<span style="text-decoration: underline">s</span>'+
                '<label class="subcheckbox">'+
                    '<input type="checkbox" id="sw-'+t[0]+'-i">'+
                    '<div class="subcheckbox-box"></div>'+
                '</label>'+
                '<span style="font-style: italic">i</span>'+
                '<label class="subcheckbox">'+
                    '<input type="checkbox" id="sw-'+t[0]+'-g">'+
                    '<div class="subcheckbox-box"></div>'+
                '</label>'+
                '<span style="font-weight: bold">g</span>';
        $(this).text('');
        $(this).append(res);
    });


    /***********************************
    *   INITIALISATION DES PARAMÈTRES  *
    ***********************************/

    jscolor.installByClassName("jscolor");
    function set_ex(sec,opt) {
        let rule = dysaide_engine.options[sec][opt][1];
        $('.ex-'+sec+'-'+opt).css({
            'color'            : ( rule.col[0] ) ? rule.col[1] : '',
            'background-color' : ( rule.bg [0] ) ? rule.bg[1]  : '',
            'text-decoration'  : ( rule.s  [0] ) ? 'underline' : '',
            'font-style'       : ( rule.i  [0] ) ? 'italic'    : '',
            'font-weight'      : ( rule.g  [0] ) ? 'bold'      : ''
        });
    }
    
    function options_to_GUI() {
        let options = dysaide_engine.options;

        /* Switches */
        for (let sec in options) {
            $('#sw-'+sec).prop('checked', options[sec].actif );
            for (let opt in options[sec]) {
                if (opt === 'actif') { continue; }
                $('#sw-'+sec+'-'+opt).prop('checked', options[sec][opt][0] );
                if ( typeof(options[sec][opt][1]) === 'object' ) {
                    for (let sub in options[sec][opt][1]) {
                        if ( typeof(options[sec][opt][1][sub]) === 'object' ) {
                            $('#sw-'+sec+'-'+opt+'-'+sub).prop('checked', options[sec][opt][1][sub][0] );
                        }
                    }
                }
            }
        }

        /* Champs */
        for (let sec in options) {
            for (let opt in options[sec]) {
                if ( opt === 'actif' ) { continue; }
                if ( typeof(options[sec][opt][1]) === 'object' ) { continue; }
                $('#'+sec+'-'+opt).val( options[sec][opt][1] );
            }
        }

        /* Couleurs */
        for (let sec in options) {
            if ( typeof(options[sec]) !== 'object' ) { continue; }
            for (let opt in options[sec]) {
                if ( typeof(options[sec][opt][1]) !== 'object' ) { continue; }
                for (let sub of ['col','bg']) {
                    for (let i = 1; i < options[sec][opt][1][sub].length; i++) {
                        document.getElementById(sec+'-'+opt+'-'+sub+'-'+i)
                            .jscolor.fromString(options[sec][opt][1][sub][i].slice(1));
                    }
                }
            }
        }
        for (let sec of ['phon','lett']) {
            for (let opt in options[sec]) {
                if (typeof(options[sec][opt])==='object' && options[sec][opt].length>1) { set_ex(sec,opt); }
            }
        }
        for (let n = options.syll.diff[1].nb+1; n < 6; n++) {
            $('#syll-diff-col-'+n).hide();
            $('#syll-diff-bg-'+n).hide();
        }

        /* +/- buttons */
        $( (options.syll.diff[1].nb === 2) ? '#moins' : '#plus' ).addClass('disabled');
        
        /* Contrôles */
        $('#ctrl-ctrl').prop('checked', ctrl.ctrl && !ctrl.maj);
        $('#ctrl-maj').prop('checked', ctrl.ctrl && ctrl.maj);
        $('#ctrl-f8').prop('checked', ctrl.f8);
        $('#ctrl-track').prop('checked', !ctrl.track);
        $('#ctrl-track-'+!ctrl.track).removeClass('actif');
        $('#ctrl-track-'+ctrl.track).addClass('actif');        
    }
    options_to_GUI();
    
    /*****************************
    *   ACTIONS DE L'INTERFACE   *
    *****************************/
    
    /* Sauvegarder */
    $('#save').click( function() {
        if ($(this).hasClass('disabled')) { return; }
        if ($('#default').hasClass('disabled')) {
            //!! Chrome
            chrome.storage.sync.remove('options');
            chrome.storage.sync.remove('ctrl');
            //!! Firefox
            //?browser.storage.sync.remove('options');
            //?browser.storage.sync.remove('ctrl');
            //!
        }
        else {
            options = $.extend(true,{},dysaide_engine.options);
            //! Chrome
            chrome.storage.sync.set({options: options, ctrl: ctrl});
            //! Firefox
            //?browser.storage.sync.set({options: options, ctrl: ctrl});
        }
        $('#save,#cancel').addClass('disabled');
    });
    /* Annuler */
    $('#cancel').click( function() {
        if ($(this).hasClass('disabled')) { return; }
        if (options) {
            dysaide_engine = new dysaide_Engine($.extend(true,{},options),true);
            $('#default').removeClass('disabled');
        }
        else {
            dysaide_engine = new dysaide_Engine(false,true);
            if (ctrl0) {
                $('#default').removeClass('disabled');
            }
        }
        ctrl = ctrl0 || {ctrl:true,maj:false,f8:true,track:true};
        $('#save,#cancel').addClass('disabled');
        options_to_GUI();
        apply();
    });
    /* Default */
    $('#default').click( function() {
        if ($(this).hasClass('disabled')) { return; }
        dysaide_engine = new dysaide_Engine(false,true);
        ctrl = {ctrl:true,maj:false,f8:true,track:true};
        if (options || ctrl0) {
            $('#save,#cancel').removeClass('disabled');
        }
        else {
            $('#save,#cancel').addClass('disabled');
        }
        $('#default').addClass('disabled');
        options_to_GUI();
        apply();
    });
    
    /* Switches on/off */
    $('[id^=sw-]').change( function(e) {
        let x = this.id.split('-');
        switch (x.length) {
            case 2:
                dysaide_engine.options[x[1]].actif = e.target.checked;
                break;
            case 3:
                dysaide_engine.options[x[1]][x[2]][0] = e.target.checked;
                break;
            default:
                dysaide_engine.options[x[1]][x[2]][1][x[3]][0] = e.target.checked;
                if ( (x[1] === 'lett') || (x[1] === 'phon') ) { set_ex(x[1],x[2]); }
        }
        if ( x.length===3 && (x[2]==='oral' || x[2]==='q_fin')) {
            if (e.target.checked) {
                if (x[2]==='oral') {
                    dysaide_engine.options.phon.q_fin[0] = false;
                    $('#sw-phon-q_fin').prop('checked',false);
                }
                else {
                    dysaide_engine.options.syll.oral[0] = false;
                    $('#sw-syll-oral').prop('checked',false);
                }
            }
            dysaide_engine = new dysaide_Engine(dysaide_engine.options,true);
            lire_texte_test();
        }
        $('.button').removeClass('disabled');
        apply();
    });
    $('#ctrl-ctrl,#ctrl-maj').change( function(e) {
        s = ((ctrl.ctrl)?'1':'0')+((ctrl.maj)?'1':'0')+((ctrl.f8)?'1':'0')
        e = e.target;
        let x = e.checked;
        if (x) {
            ctrl.maj = (this.id === 'ctrl-maj');
            if (ctrl.ctrl) {
                $('#ctrl-'+((ctrl.maj)?'ctrl':'maj')).prop('checked', false);
            }
            else {
                ctrl.ctrl = true;
            }
        }
        else {
            ctrl.ctrl = false;
            if (!ctrl.f8) {
                ctrl.f8 = true;
                $('#ctrl-f8').prop('checked', true);
            }
        }
        $('.button').removeClass('disabled');
    });
    $('#ctrl-f8').change( function(e) {
        e = e.target.checked;
        ctrl.f8 = e;
        if (!(e||ctrl.ctrl)) {
            ctrl.ctrl = true;
            $('#ctrl-'+((ctrl.maj)?'maj':'ctrl')).prop('checked', true);
        }
        $('.button').removeClass('disabled');
    });
    $('#ctrl-track').change( function(e) {
        ctrl.track = !e.target.checked;
        $('#ctrl-track-'+!ctrl.track).removeClass('actif');
        $('#ctrl-track-'+ctrl.track).addClass('actif');        
        $('.button').removeClass('disabled');
    });

    /* Champ */
    $('input[type=number], select').bind('input change', function() {
        dysaide_engine.options[this.id.substr(0,4)][this.id.substr(5)][1] = this.value;
        if (this.id === 'incl-sep' || this.id === 'syll-sep') { dysaide_engine = new dysaide_Engine(dysaide_engine.options,true); }
        $('.button').removeClass('disabled');
        apply();
    });

    /* Couleurs */
    $('.jscolor').change( function() {
        let x = this.id.split('-');
        dysaide_engine.options[x[0]][x[1]][1][x[2]][x[3]] = '#'+this.jscolor;
        if ( (x[0] === 'lett') || (x[0] === 'phon') ) { set_ex(x[0],x[1]); }
        $('.button').removeClass('disabled');
        apply();
    });

    /* Boutons +/- pour les syllabes */
    $('#moins').click( function() {
        $('#moins').addClass('disabled');
        $('#plus').removeClass('disabled');
        $('#syll-diff-col-3').hide();
        $('#syll-diff-bg-3').hide();    
        dysaide_engine.options.syll.diff[1].nb = 2;
        $('.button').removeClass('disabled');
        apply();
    });
    $('#plus').click( function() {
        $('#plus').addClass('disabled');
        $('#moins').removeClass('disabled');
        $('#syll-diff-col-3').show();
        $('#syll-diff-bg-3').show();    
        dysaide_engine.options.syll.diff[1].nb = 3;
        $('.button').removeClass('disabled');
        apply();
    });


    /*******************************************
    *   SÉQUENÇAGE DU TEXTE DE DÉMONSTRATION   *
    *******************************************/

    function lire_texte_test() {
        let t = $('#test-text').text();
        $('#test-text').html( dysaide_engine.remplace_texte(t) );
        $('#test-text').prop('hist',t);
    }
    lire_texte_test();

    $('#test-text').on( 'blur', function() {
        if ( $('#test-text').text() !== $('#test-text').prop('hist') ) {
            lire_texte_test();
            apply();
        }
    });

    /******************************
    *   APPLICATION DES OPTIONS   *
    ******************************/
    
    let style_el = document.createElement('style');
    document.body.appendChild(style_el);

    function apply() {
        $('body').css( 'font-family', dysaide_engine.options.gene.police[1] );
        let oral = dysaide_engine.options.syll.oral[0];
        $('#syll-oral-'+!oral).removeClass('actif');
        $('#syll-oral-'+oral).addClass('actif');
        if (oral) { $('.syll-oral').hide(); } else { $('.syll-oral').show(); }
        if (dysaide_engine.options.phon.q_fin[0]) {
            $('.e-fin').removeClass('ex-phon-H').addClass('ex-phon-q');
            set_ex('phon','q');
        }
        else {
            $('.e-fin').removeClass('ex-phon-q').addClass('ex-phon-H');
            set_ex('phon','H');
        }
        let css = dysaide_engine.css();
        if ( dysaide_engine.options.gene.actif ) {
            let rule = dysaide_engine.options.gene;
            let css_rules = [];
            if ( rule.police[0]      ) { css_rules.push( 'font-family:"'   +         rule.police[1]         + '"  !important' ); }
            if ( rule.esp_lettres[0] ) { css_rules.push( 'letter-spacing:' +         rule.esp_lettres[1]    + 'em !important' ); }
            if ( rule.esp_mots[0]    ) { css_rules.push( 'word-spacing:'   +         rule.esp_mots[1]       + 'em !important' ); }
            if ( this.testmode || css_rules.length>0 ) { css += '.match-ex{'+css_rules.join(';')+'}'; }
        }
        style_el.textContent = css;
        // Augmenter la taille de l'entête si besoin
        window.setTimeout(head_size, 100);
        window.setTimeout(() => dysaide_engine.colore_lignes(), 100);
    }
    apply();

    function head_size() {
        let h = $('#header').height();
        $('.buttons>.header-ghost').height(h);
        let H = $('.main>.header-ghost').height();
        if ( h > H ) {
            if (H>0) { window.scrollBy(0,h-H); }
            $('.main>.header-ghost').height(h);
        }
    }

    let resize_timeout;
    $(window).on("resize", function(){
        window.clearTimeout(resize_timeout);
        resize_timeout = window.setTimeout(() => dysaide_engine.colore_lignes(), 100);
        head_size();
    });

});
